#ifndef __STORAGE_H
#define __STORAGE_H

#include <chat_types.h>

void storage_init();
void storage_finalize();

int storage_load_chats(Chat **chats);
void storage_free_chats(Chat *chats, int n_chats);

gboolean storage_add_chat(const char *eid);
gboolean storage_remove_chat(const char *eid);

gboolean storage_add_message(const char *eid, Message *msg);

#endif // __STORAGE_H
