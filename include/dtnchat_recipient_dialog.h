#ifndef __DTNCHATRECIPIENTDIALOG_H
#define __DTNCHATRECIPIENTDIALOG_H

#include <gtk/gtk.h>

#include <dtnchat_window.h>

#define DTNCHAT_RECIPIENT_DIALOG_TYPE (dtnchat_recipient_dialog_get_type())
G_DECLARE_FINAL_TYPE(DTNChatRecipientDialog, dtnchat_recipient_dialog, DTNCHAT, RECIPIENT_DIALOG, GtkDialog);

DTNChatRecipientDialog *dtnchat_recipient_dialog_new();
GtkEntry *dtnchat_recipient_dialog_get_entry(DTNChatRecipientDialog *dialog);

#endif // __DTNCHATRECIPIENTDIALOG_H
