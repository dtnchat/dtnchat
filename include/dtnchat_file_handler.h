#ifndef __DTNCHAT_FILE_HANDLER_H
#define __DTNCHAT_FILE_HANDLER_H

#include <gtk/gtk.h>

#define DTNCHAT_FILE_HANDLER_TYPE (dtnchat_file_handler_get_type())
G_DECLARE_FINAL_TYPE(DTNChatFileHandler, dtnchat_file_handler, DTNCHAT, FILE_HANDLER, GtkBox);

DTNChatFileHandler *dtnchat_file_handler_new(const char *file_path, const char *logical_filename);

#endif // __DTNCHAT_FILE_HANDLER_H
