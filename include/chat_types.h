#ifndef __CHAT_TYPES_H
#define __CHAT_TYPES_H

#include <time.h>

typedef enum {
    MESSAGE_DIRECTION_OUT = 0,
    MESSAGE_DIRECTION_IN
} MessageDirection;

typedef char MessageType;
#define MESSAGE_TYPE_TEXT 't'
#define MESSAGE_TYPE_PHOTO 'p'
#define MESSAGE_TYPE_AUDIO 'a'
#define MESSAGE_TYPE_FILE 'f'

typedef struct {
    MessageDirection direction;
    MessageType type;
    time_t time;
    char *text;
} Message;

typedef struct {
    char *eid;
    Message *messages;
    size_t n_messages;
} Chat;

#endif // __CHAT_TYPES_H
