#ifndef __DTNCHATVIEW_H
#define __DTNCHATVIEW_H

#include <gtk/gtk.h>

#include <storage.h>

#define DTNCHAT_VIEW_TYPE (dtnchat_view_get_type())
G_DECLARE_FINAL_TYPE(DTNChatView, dtnchat_view, DTNCHAT, VIEW, GtkGrid);

DTNChatView *dtnchat_view_new(Chat *chat);
void dtnchat_view_add_message(DTNChatView *chat_view, const Message *msg);

#endif // __DTNCHATVIEW_H
