#ifndef __BUNDLE_H
#define __BUNDLE_H

#include <chat_types.h>

#define APP_DEMUX "dtnchat"
#define APP_SERVICE_NUMBER 1329

typedef void (*MessageCallback)(gpointer user_data, char *eid, Message *msg);

int bundle_init(int argc, char **argv);
int bundle_send_message(const char *dest_eid, Message *msg);
void bundle_start_receiving(MessageCallback callback, gpointer user_data);
void bundle_finalize();

#endif // __BUNDLE_H
