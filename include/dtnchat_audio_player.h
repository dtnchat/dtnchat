#ifndef __DTNCHAT_AUDIO_PLAYER_H
#define __DTNCHAT_AUDIO_PLAYER_H

#include <gtk/gtk.h>

#define DTNCHAT_AUDIO_PLAYER_TYPE (dtnchat_audio_player_get_type())
G_DECLARE_FINAL_TYPE(DTNChatAudioPlayer, dtnchat_audio_player, DTNCHAT, AUDIO_PLAYER, GtkBox);

DTNChatAudioPlayer *dtnchat_audio_player_new(const char *file_path);

#endif // __DTNCHAT_AUDIO_PLAYER_H
