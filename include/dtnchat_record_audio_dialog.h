#ifndef __DTNCHAT_RECORD_AUDIO_DIALOG_H
#define __DTNCHAT_RECORD_AUDIO_DIALOG_H

#include <gtk/gtk.h>

#define DTNCHAT_RECORD_AUDIO_DIALOG_TYPE (dtnchat_record_audio_dialog_get_type())
G_DECLARE_FINAL_TYPE(DTNChatRecordAudioDialog, dtnchat_record_audio_dialog, DTNCHAT, RECORD_AUDIO_DIALOG, GtkDialog);

DTNChatRecordAudioDialog *dtnchat_record_audio_dialog_new(char *file_path);
char *dtnchat_record_audio_dialog_get_file_path(DTNChatRecordAudioDialog *dialog);

#endif // __DTNCHAT_RECORD_AUDIO_DIALOG_H
