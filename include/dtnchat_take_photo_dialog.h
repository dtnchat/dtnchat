#ifndef __DTNCHAT_TAKE_PHOTO_DIALOG_H
#define __DTNCHAT_TAKE_PHOTO_DIALOG_H

#include <gtk/gtk.h>

#define DTNCHAT_TAKE_PHOTO_DIALOG_TYPE (dtnchat_take_photo_dialog_get_type())
G_DECLARE_FINAL_TYPE(DTNChatTakePhotoDialog, dtnchat_take_photo_dialog, DTNCHAT, TAKE_PHOTO_DIALOG, GtkWindow);

DTNChatTakePhotoDialog *dtnchat_take_photo_dialog_new(char *file_path);

#endif // __DTNCHAT_TAKE_PHOTO_DIALOG_H
