#ifndef __DTNCHAT_SAVE_FILE_BUTTON_H
#define __DTNCHAT_SAVE_FILE_BUTTON_H

#include <gtk/gtk.h>

#define DTNCHAT_SAVE_FILE_BUTTON_TYPE (dtnchat_save_file_button_get_type())
G_DECLARE_FINAL_TYPE(DTNChatSaveFileButton, dtnchat_save_file_button, DTNCHAT, SAVE_FILE_BUTTON, GtkButton);

DTNChatSaveFileButton *dtnchat_save_file_button_new(char *file_path, char *logical_filename, gboolean icon_only);

#endif // __DTNCHAT_SAVE_FILE_BUTTON_H
