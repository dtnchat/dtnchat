#ifndef __DTNCHATAPP_H
#define __DTNCHATAPP_H

#include <gtk/gtk.h>

#define DTNCHAT_APP_TYPE (dtnchat_app_get_type())
G_DECLARE_FINAL_TYPE(DTNChatApp, dtnchat_app, DTNCHAT, APP, GtkApplication)

DTNChatApp *dtnchat_app_new(void);

#endif // __DTNCHATAPP_H
