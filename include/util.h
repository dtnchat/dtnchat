#ifndef __UTIL_H
#define __UTIL_H

#include <stddef.h>
#include <gtk/gtk.h>

char *escape_text(const char *text, const char *escape_chars);
char *unescape_text(const char *text);

char *get_dtnchat_home_dir(void);

char *get_file_name_from_path(char *path);
char *get_subdir_path(const char *subdir, const char *eid);

int get_path_for_photo(char **dest, const char *eid);
int get_path_for_audio(char **dest, const char *eid);
int get_path_for_file(char **dest, const char *eid);

int get_msg_dir_path(char **dest);

char *get_existing_photo_path(const char *filename, const char *eid);
char *get_existing_audio_path(const char *filename, const char *eid);
char *get_existing_file_path(const char *filename, const char *eid);

char *split_file_string_pair(char *str);
void show_error_dialog(GtkWidget *widget, const char *msg);

#endif // __UTIL_H
