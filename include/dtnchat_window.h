#ifndef __DTNCHATWINDOW_H
#define __DTNCHATWINDOW_H

#include <gtk/gtk.h>
#include <dtnchat_app.h>

#define DTNCHAT_WINDOW_TYPE (dtnchat_window_get_type())
G_DECLARE_FINAL_TYPE(DTNChatWindow, dtnchat_window, DTNCHAT, WINDOW, GtkApplicationWindow);

DTNChatWindow *dtnchat_window_new(DTNChatApp *app);

#endif // __DTNCHATWINDOW_H
