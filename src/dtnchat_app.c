#include <stdio.h>

#include <dtnchat_app.h>
#include <dtnchat_window.h>
#include <bundle.h>
#include <storage.h>

struct _DTNChatApp {
    GtkApplication parent;
};

G_DEFINE_TYPE(DTNChatApp, dtnchat_app, GTK_TYPE_APPLICATION);

static void dtnchat_app_init(DTNChatApp *app) {}

// Called when the application is activated
static void dtnchat_app_activate(GApplication *app) {
    DTNChatWindow *win;

    win = dtnchat_window_new(DTNCHAT_APP(app));
    gtk_window_present(GTK_WINDOW(win));
}

static void dtnchat_app_shutdown(GApplication *obj) {
    bundle_finalize();
    storage_finalize();

    // Chain up
    G_APPLICATION_CLASS(dtnchat_app_parent_class)->shutdown(obj);
}

static void dtnchat_app_class_init(DTNChatAppClass *class) {
    G_APPLICATION_CLASS(class)->activate = dtnchat_app_activate;
    G_APPLICATION_CLASS(class)->shutdown = dtnchat_app_shutdown;
}

DTNChatApp *dtnchat_app_new(void) {
    return g_object_new(DTNCHAT_APP_TYPE,
            "application-id", "it.unibo.dtnchat",
            NULL);
}
