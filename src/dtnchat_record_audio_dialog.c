#include <dtnchat_record_audio_dialog.h>
#include <dtnchat_audio_player.h>
#include <util.h>

#include <gtk/gtk.h>
#include <gst/gst.h>
#include <unistd.h>

struct _DTNChatRecordAudioDialog {
    GtkDialog parent;
    GtkWidget *media_box;
    GtkWidget *button_record;
    GtkWidget *media_button_box;
    GtkWidget *button_pause;
    GtkWidget *button_play;
    GtkWidget *button_stop;
    GtkWidget *status_box;
    GtkWidget *image_record;
    GtkWidget *image_paused;
    GtkWidget *recording_label;
    GtkWidget *button_cancel;
    GtkWidget *button_ok;

    GstElement *pipeline;
    GstElement *source;
    GstElement *queue;
    GstElement *convert;
    GstElement *encode;
    GstElement *mux;
    GstElement *sink;

    char *file_path;
};

G_DEFINE_TYPE(DTNChatRecordAudioDialog, dtnchat_record_audio_dialog, GTK_TYPE_DIALOG);

#define PROP_FILE_PATH 1

static void dtnchat_record_audio_dialog_init(DTNChatRecordAudioDialog *dialog) {
    gtk_widget_init_template(GTK_WIDGET(dialog));
}

static void dtnchat_record_audio_dialog_set_property(GObject *obj, guint property_id, const GValue *val, GParamSpec *pspec) {
    DTNChatRecordAudioDialog *dialog = DTNCHAT_RECORD_AUDIO_DIALOG(obj);

    if (property_id == PROP_FILE_PATH) {
        g_free(dialog->file_path);
        dialog->file_path = g_value_dup_string(val);
    } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_record_audio_dialog_get_property(GObject *obj, guint property_id, GValue *val, GParamSpec *pspec) {
    DTNChatRecordAudioDialog *dialog = DTNCHAT_RECORD_AUDIO_DIALOG(obj);

    if (property_id == PROP_FILE_PATH) {
        g_value_set_string(val, dialog->file_path);
    } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_record_audio_dialog_finalize(GObject *obj) {
    DTNChatRecordAudioDialog *dialog = DTNCHAT_RECORD_AUDIO_DIALOG(obj);
    g_free(dialog->file_path);
}

static void dtnchat_record_audio_dialog_class_init(DTNChatRecordAudioDialogClass *class) {
    GParamSpec *prop_file_path;

    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class), "/it/unibo/dtnchat/record_audio_dialog.ui");
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, media_box);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, button_record);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, media_button_box);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, button_pause);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, button_play);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, button_stop);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, status_box);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, image_record);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, image_paused);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, recording_label);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, button_cancel);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecordAudioDialog, button_ok);

    G_OBJECT_CLASS(class)->set_property = dtnchat_record_audio_dialog_set_property;
    G_OBJECT_CLASS(class)->get_property = dtnchat_record_audio_dialog_get_property;
    G_OBJECT_CLASS(class)->finalize = dtnchat_record_audio_dialog_finalize;

    prop_file_path = g_param_spec_string("file-path",
                                         "FilePath",
                                         "Path of the file to record to",
                                         NULL,
                                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

    g_object_class_install_property(G_OBJECT_CLASS(class), PROP_FILE_PATH, prop_file_path);

    g_param_spec_unref(prop_file_path);
}

DTNChatRecordAudioDialog *dtnchat_record_audio_dialog_new(char *file_path) {
    return g_object_new(DTNCHAT_RECORD_AUDIO_DIALOG_TYPE,
            "use-header-bar", 1,
            "file-path", file_path,
            NULL);
}

// DTNChat makes use of the following GStreamer pipeline for audio recording:
// alsasrc ! audioconvert ! vorbisenc ! oggmux ! filesink location=${file_path}
void dtnchat_record_audio_dialog_record(DTNChatRecordAudioDialog *dialog) {
    GstStateChangeReturn ret;

    dialog->source = gst_element_factory_make("alsasrc", "source");
    dialog->queue = gst_element_factory_make("queue", "queue");
    dialog->convert = gst_element_factory_make("audioconvert", "convert");
    dialog->encode = gst_element_factory_make("vorbisenc", "encode");
    dialog->mux = gst_element_factory_make("oggmux", "mux");
    dialog->sink = gst_element_factory_make("filesink", "sink");
    g_object_set(dialog->sink, "location", dialog->file_path, NULL);

    dialog->pipeline = gst_pipeline_new("pipeline");

    gst_bin_add_many(GST_BIN(dialog->pipeline),
                     dialog->source,
                     dialog->queue,
                     dialog->convert,
                     dialog->encode,
                     dialog->mux,
                     dialog->sink,
                     NULL);

    if (gst_element_link_many(dialog->source,
                              dialog->queue,
                              dialog->convert,
                              dialog->encode,
                              dialog->mux,
                              dialog->sink,
                              NULL) != TRUE) {
        fprintf(stderr, "%s: error linking GStreamer elements.\n", __func__);
        show_error_dialog(GTK_WIDGET(dialog), "Error: unable to record (failed linking GStreamer elements)");
        return;
    }

    ret = gst_element_set_state(dialog->pipeline, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "%s: error setting the pipeline to the PLAYING state.\n", __func__);
        show_error_dialog(GTK_WIDGET(dialog), "Error: unable to record (failed setting the pipeline to the PLAYING state)");
        return;
    } else if (ret == GST_STATE_CHANGE_ASYNC) {
        gst_element_get_state(dialog->pipeline, NULL, NULL, GST_CLOCK_TIME_NONE);
    }

    gtk_widget_destroy(dialog->button_record);

    gtk_container_add(GTK_CONTAINER(dialog->media_box), dialog->status_box);
    gtk_container_add(GTK_CONTAINER(dialog->media_box), dialog->media_button_box);

    gtk_widget_show_all(dialog->media_box);
}

void dtnchat_record_audio_dialog_pause(DTNChatRecordAudioDialog *dialog) {
    GstStateChangeReturn ret;

    ret = gst_element_set_state(dialog->pipeline, GST_STATE_PAUSED);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "%s: unable to set pipeline to PLAYING\n", __func__);
        return;
    }

    gtk_widget_hide(dialog->button_pause);
    gtk_widget_show(dialog->button_play);
    gtk_label_set_label(GTK_LABEL(dialog->recording_label), "Paused");
    gtk_widget_hide(dialog->image_record);
    gtk_widget_show(dialog->image_paused);
}

void dtnchat_record_audio_dialog_play(DTNChatRecordAudioDialog *dialog) {
    GstStateChangeReturn ret;

    ret = gst_element_set_state(dialog->pipeline, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "%s: unable to set pipeline state to PAUSED\n", __func__);
        return;
    }

    gtk_widget_show(dialog->button_pause);
    gtk_widget_hide(dialog->button_play);
    gtk_label_set_label(GTK_LABEL(dialog->recording_label), "Recording");
    gtk_widget_show(dialog->image_record);
    gtk_widget_hide(dialog->image_paused);
}

void dtnchat_record_audio_dialog_stop(DTNChatRecordAudioDialog *dialog) {
    DTNChatAudioPlayer *player;
    GstStateChangeReturn ret;

    ret = gst_element_set_state(dialog->pipeline, GST_STATE_NULL);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "%s: unable to set pipeline state to NULL\n", __func__);
        return;
    }
    gtk_widget_set_sensitive(dialog->button_ok, TRUE);

    player = dtnchat_audio_player_new(dialog->file_path);
    gtk_container_add(GTK_CONTAINER(dialog->media_box), GTK_WIDGET(player));

    gtk_widget_destroy(dialog->status_box);
    gtk_widget_destroy(dialog->media_button_box);

    gtk_widget_show(GTK_WIDGET(player));
}

void dtnchat_record_audio_dialog_cancel(DTNChatRecordAudioDialog *dialog) {
    unlink(dialog->file_path);
    gtk_widget_destroy(GTK_WIDGET(dialog));
}

char *dtnchat_record_audio_dialog_get_file_path(DTNChatRecordAudioDialog *dialog) {
    return dialog->file_path;
}
