#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <glib.h>

#include <storage.h>
#include <util.h>

typedef struct {
    char *eid;
    int fd;
} FDEntry;

static GArray *fds = NULL;

static int storage_load_messages(const char *file_path, Message **messages);
static int storage_open_existing_chat(char *file_path);
static void storage_free_messages(Message *messages, int n_messages);

void storage_init() {
    fds = g_array_new(FALSE, FALSE, sizeof(FDEntry));
}

int storage_load_chats(Chat **chats) {
    char *msg_dir_path;
    DIR *dir;
    struct dirent *dirent;
    int chat_count = 0;

    if (get_msg_dir_path(&msg_dir_path) < 0) {
        fprintf(stderr, "Storage: could not get path for message directory\n");
        return -1;
    }

    dir = opendir(msg_dir_path);
    if (dir == NULL) {
        perror("Storage: could not open message directory");
        return -1;
    }

    // Count number of files in msg directory (which corresponds to the number of chats)
    errno = 0;
    while ((dirent = readdir(dir)) != NULL) {
        if (strcmp(dirent->d_name, ".") == 0 || strcmp(dirent->d_name, "..") == 0)
            continue;
        chat_count++;
    }
    if (errno != 0) {
        perror("Storage: could not read message directory");
        closedir(dir);
        return -1;
    }

    // Allocate resulting array
    *chats = malloc(chat_count * sizeof(Chat));
    rewinddir(dir);
    chat_count = 0;

    // Fill resulting array
    errno = 0;
    while ((dirent = readdir(dir)) != NULL) {
        int n_messages;
        Message *messages;
        char *chat_path;

        if (strcmp(dirent->d_name, ".") == 0 || strcmp(dirent->d_name, "..") == 0)
            continue;

        // Obtain path to the chat file
        chat_path = malloc(strlen(msg_dir_path) + strlen(dirent->d_name) + 2);
        sprintf(chat_path, "%s/%s", msg_dir_path, dirent->d_name);

        // Read messages
        n_messages = storage_load_messages(chat_path, &messages);
        if (n_messages < 0) {
            storage_free_chats(*chats, chat_count);
            return -1;
        }

        storage_open_existing_chat(chat_path);
        free(chat_path);

        (*chats)[chat_count].eid = unescape_text(dirent->d_name);
        (*chats)[chat_count].n_messages = n_messages;
        (*chats)[chat_count].messages = messages;

        chat_count++;
    }
    if (errno != 0) {
        perror("Storage: could not read message directory");
        storage_free_chats(*chats, chat_count);
        return -1;
    }

    closedir(dir);

    return chat_count;
}

static int storage_load_messages(const char *file_path, Message **msgs) {
    FILE *f;
    int ch, previous_ch;
    int message_count;

    Message *messages;
    int i, scanf_ret;
    char direction_char;
    char time[20];
    int text_length;

    f = fopen(file_path, "r");
    if (!f) {
        fprintf(stderr, "Storage: could not open file %s in readonly mode: ", file_path);
        perror(NULL);
        return -1;
    }

    // Count number of lines (which corresponds to the number of messages)
    message_count = 0;
    previous_ch = '\0';
    while ((ch = fgetc(f)) != EOF) {
        if (ch == '\n' && previous_ch != '\n') // Skip consecutive newlines
            message_count++;

        previous_ch = ch;
    }

    // Allocate resulting array
    messages = malloc(message_count * sizeof(Message));
    fseek(f, 0, SEEK_SET);

    // Fill resulting array by reading line by line
    for (i = 0; (scanf_ret = fscanf(f, " %c%c %s %d ", &direction_char, &messages[i].type, time, &text_length)) == 4; i++) {
        struct tm utc_time;
        struct tm dt;
        time_t tzlag;
        char *escaped_text;

        // Set direction based on char in file
        if (direction_char == '>') {
            messages[i].direction = MESSAGE_DIRECTION_OUT;
        } else if (direction_char == '<') {
            messages[i].direction = MESSAGE_DIRECTION_IN;
        } else {
            fprintf(stderr, "Storage: invalid message direction: %c\n", direction_char);
            storage_free_messages(messages, i);
            fclose(f);
            return -1;
        }

        switch (messages[i].type) {
            case MESSAGE_TYPE_TEXT:
            case MESSAGE_TYPE_PHOTO:
            case MESSAGE_TYPE_AUDIO:
            case MESSAGE_TYPE_FILE:
                break; // ok
            default:
                fprintf(stderr, "%s: invalid message type: %c\n", __func__, messages[i].type);
                storage_free_messages(messages, i);
                fclose(f);
                return -1;
        }

        // Get timezone lag
        memset(&dt, 0, sizeof(struct tm));
        dt.tm_mday = 1;
        dt.tm_year = 70;
        tzlag = mktime(&dt);
        if (tzlag == -1) {
            perror("Storage: mktime");
            storage_free_messages(messages, i);
            fclose(f);
            return -1;
        }

        // Parse message time
        strptime(time, "%FT%T", &utc_time);
        utc_time.tm_isdst = 0; // Ignore daylight saving
        messages[i].time = mktime(&utc_time);
        if (messages[i].time == -1) {
            perror("Storage: mktime");
            storage_free_messages(messages, i);
            fclose(f);
            return -1;
        }
        messages[i].time -= tzlag;

        // Get text and unescape it
        escaped_text = malloc((text_length + 1) * sizeof(char));
        if (!fgets(escaped_text, text_length + 1, f)) {
            perror("Storage: fgets");
            storage_free_messages(messages, i);
            fclose(f);
            return -1;
        }
        messages[i].text = unescape_text(escaped_text);
        free(escaped_text);
    }

    if (scanf_ret > 0) {
        fprintf(stderr, "Storage: invalid number of tokens in line\n");
        fclose(f);
        storage_free_messages(messages, i - 1);
        return -1;
    }

    if (fclose(f) == EOF) {
        fprintf(stderr, "Storage: couldn't close file %s: ", file_path);
        perror(NULL);

        storage_free_messages(messages, message_count);
        return -1;
    }

    *msgs = messages;
    return message_count;
}

// Open file and store it in the file descriptor array
static int storage_open_existing_chat(char *file_path) {
    FDEntry entry;

    entry.fd = open(file_path, O_WRONLY);
    if (entry.fd < 0) {
        fprintf(stderr, "Storage: couldn't open file %s in writeonly mode: ", file_path);
        perror(NULL);
        return -1;
    }

    lseek(entry.fd, 0, SEEK_END);

    entry.eid = unescape_text(get_file_name_from_path(file_path));

    g_array_append_val(fds, entry);

    return 0;
}

static void storage_free_messages(Message *messages, int n_messages) {
    int i;

    for (i = 0; i < n_messages; i++) {
        g_free(messages[i].text);
    }

    g_free(messages);
}

void storage_free_chats(Chat *chats, int n_chats) {
    int i;

    for (i = 0; i < n_chats; i++) {
        g_free(chats[i].eid);

        storage_free_messages(chats[i].messages, chats[i].n_messages);
    }

    g_free(chats);
}

static int storage_fd_from_eid(const char *eid) {
    FDEntry *data = (FDEntry *) fds->data;

    for (int i = 0; i < fds->len; i++) {
        if (strcmp(eid, data[i].eid) == 0) {
            return data[i].fd;
        }
    }

    return -1;
}

int get_path_for_chat(char **dest, const char *eid) {
    char *home_dir_path;
    char *escaped_eid;
    static char file_path[4096];

    home_dir_path = get_dtnchat_home_dir();

    if (!home_dir_path) {
        return -1;
    }

    escaped_eid = escape_text(eid, "/"); // Escape slashes because they're not valid in file names

    snprintf(file_path, sizeof(file_path), "%s/msg/%s", home_dir_path, escaped_eid);

    free(escaped_eid);

    *dest = file_path;

    return 0;
}

gboolean storage_add_chat(const char *eid) {
    char *file_path;
    char *subdir_path;
    char *subdirs[] = { "photo", "audio", "file" };
    size_t len_subdirs = sizeof(subdirs) / sizeof(*subdirs);
    FDEntry entry;

    if (get_path_for_chat(&file_path, eid) < 0) {
        fprintf(stderr, "Storage: could not get path for chat\n");
        return FALSE;
    }

    entry.fd = open(file_path, O_WRONLY | O_CREAT, 0644);
    if (entry.fd < 0) {
        perror("Storage: could not create file");
        return FALSE;
    }

    entry.eid = g_strdup(eid);

    for (int i = 0; i < len_subdirs; i++) {
        subdir_path = get_subdir_path(subdirs[i], eid);
        if (!subdir_path) {
            for (int j = 0; j < i; j++) {
                subdir_path = get_subdir_path(subdirs[j], eid);
                rmdir(subdir_path);
            }
            close(entry.fd);
            unlink(file_path);
            g_free(entry.eid);
            return FALSE;
        }
        if (mkdir(subdir_path, 0755) < 0 && errno != EEXIST) {
            fprintf(stderr, "Storage: could not create directory %s: ", subdir_path);
            perror(NULL);
            for (int j = 0; j < i; j++) {
                subdir_path = get_subdir_path(subdirs[j], eid);
                rmdir(subdir_path);
            }
            close(entry.fd);
            unlink(file_path);
            g_free(entry.eid);
            return FALSE;
        }
    }

    g_array_append_val(fds, entry);
    return TRUE;
}

gboolean storage_remove_chat(const char *eid) {
    FDEntry *data = (FDEntry *) fds->data;
    int index = -1;
    Message *messages;
    int n_messages;
    char *chat_path;
    char *filename_pair;
    char *path_to_delete;
    char *subdirs[] = { "photo", "audio", "file" };
    size_t len_subdirs = sizeof(subdirs) / sizeof(*subdirs);

    for (int i = 0; i < fds->len; i++) {
        if (strcmp(data[i].eid, eid) == 0) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        fprintf(stderr, "Storage: error removing chat: no such chat is open\n");
        return FALSE;
    }

    // First close the chat file
    if (close(data[index].fd) < 0) {
        fprintf(stderr, "Storage: could not close chat file %s: ", eid);
        perror(NULL);
        return FALSE;
    }

    get_path_for_chat(&chat_path, eid);

    // Then read it and remove all referenced files
    n_messages = storage_load_messages(chat_path, &messages);
    if (n_messages < 0) {
        return FALSE;
    }

    for (int i = 0; i < n_messages; i++) {
        switch (messages[i].type) {
            case MESSAGE_TYPE_PHOTO:
                path_to_delete = get_existing_photo_path(messages[i].text, eid);
                break;

            case MESSAGE_TYPE_AUDIO:
                path_to_delete = get_existing_audio_path(messages[i].text, eid);
                break;

            case MESSAGE_TYPE_FILE:
                filename_pair = g_strdup(messages[i].text);
                if (!split_file_string_pair(filename_pair)) {
                    fprintf(stderr, "Storage: invalid file message filename pair\n");
                    return FALSE;
                }
                path_to_delete = get_existing_file_path(filename_pair, eid);
                g_free(filename_pair);
                break;

            default:
                continue;
        }

        if (unlink(path_to_delete) < 0) {
            fprintf(stderr, "Storage: failed deleting file %s: ", path_to_delete);
            perror(NULL);
        }
        g_free(path_to_delete);
    }

    free(messages);

    for (int i = 0; i < len_subdirs; i++) {
        path_to_delete = get_subdir_path(subdirs[i], eid);
        if (!path_to_delete) {
            continue;
        }
        rmdir(path_to_delete);
    }

    if (unlink(chat_path) < 0) {
        fprintf(stderr, "Storage: could not remove chat file %s: ", chat_path);
        perror(NULL);
        return FALSE;
    }

    g_array_remove_index_fast(fds, index);
    return TRUE;
}

gboolean storage_add_message(const char *eid, Message *msg) {
    int fd;
    struct tm *utc_time;
    char time_str[30];
    char *escaped_text;

    if ((fd = storage_fd_from_eid(eid)) < 0) { // If the chat does not exist, create it
        if (!storage_add_chat(eid)) {
            return FALSE;
        }
    }

    utc_time = gmtime(&msg->time);
    strftime(time_str, sizeof(time_str), "%FT%T", utc_time);

    escaped_text = escape_text(msg->text, "\n");

    dprintf(fd, "%c%c %s %lu %s\n", msg->direction == MESSAGE_DIRECTION_OUT ? '>' : '<', msg->type, time_str, (long unsigned int) strlen(escaped_text), escaped_text);

    free(escaped_text);

    return TRUE;
}

void storage_finalize() {
    FDEntry *data = (FDEntry *) fds->data;

    for (int i = 0; i < fds->len; i++) {
        close(data[i].fd);
        g_free(data[i].eid);
    }

    g_array_unref(fds);
    fds = NULL;
}
