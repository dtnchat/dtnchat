#include <gtk/gtk.h>
#include <gst/gst.h>
#include <cheese/cheese-gtk.h>

#include <dtnchat_app.h>
#include <bundle.h>
#include <storage.h>

int main(int argc, char **argv) {
    if (bundle_init(argc, argv) < 0) {
        exit(1);
    }
    storage_init();

    gst_init(NULL, NULL);

    if (!cheese_gtk_init(NULL, NULL)) {
        fprintf(stderr, "Unable to initialize libcheese-gtk.\n");
        exit(1);
    }

    return g_application_run(G_APPLICATION(dtnchat_app_new()), 0, NULL);
}
