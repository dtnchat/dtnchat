#include <gtk/gtk.h>
#include <gst/gst.h>

#include <dtnchat_audio_player.h>
#include <util.h>

struct _DTNChatAudioPlayer {
    GtkBox parent;
    GtkWidget *play_pause_button;
    GtkLabel *current_time_label;
    GtkLabel *total_time_label;
    GtkWidget *scale;
    GtkAdjustment *adjustment;
    GtkWidget *play_image;
    GtkWidget *pause_image;

    GstElement *pipeline;
    GstElement *source;
    GstElement *demux;
    GstElement *decode;
    GstElement *convert;
    GstElement *sink;

    gulong scale_value_changed_signal_id;
    char *file_path;
};

G_DEFINE_TYPE(DTNChatAudioPlayer, dtnchat_audio_player, GTK_TYPE_BOX);

#define PROP_FILE_PATH 1

static void pad_added_handler(GstElement *src, GstPad *new_pad, DTNChatAudioPlayer *player);
static void end_of_stream_handler(GstBus *bus, GstMessage *msg, DTNChatAudioPlayer *player);
static gboolean refresh_ui(DTNChatAudioPlayer *player);
void dtnchat_audio_player_toggle_pause(DTNChatAudioPlayer *player);
void dtnchat_audio_player_scale_value_changed(DTNChatAudioPlayer *player);
static void nanoseconds_to_string(char *dest, size_t max_len, gint64 nanoseconds);

static void dtnchat_audio_player_init(DTNChatAudioPlayer *player) {
    gtk_widget_init_template(GTK_WIDGET(player));
}

static void dtnchat_audio_player_set_property(GObject *obj, guint property_id, const GValue *val, GParamSpec *pspec) {
    DTNChatAudioPlayer *player = DTNCHAT_AUDIO_PLAYER(obj);

    if (property_id == PROP_FILE_PATH) {
        g_free(player->file_path);
        player->file_path = g_value_dup_string(val);
    } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_audio_player_get_property(GObject *obj, guint property_id, GValue *val, GParamSpec *pspec) {
    DTNChatAudioPlayer *player = DTNCHAT_AUDIO_PLAYER(obj);

    if (property_id == PROP_FILE_PATH) {
        g_value_set_string(val, player->file_path);
    } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_audio_player_constructed(GObject *obj) {
    DTNChatAudioPlayer *player = DTNCHAT_AUDIO_PLAYER(obj);
    GstBus *bus;

    player->source = gst_element_factory_make("filesrc", "source");
    player->demux = gst_element_factory_make("oggdemux", "demux");
    player->decode = gst_element_factory_make("vorbisdec", "decode");
    player->convert = gst_element_factory_make("audioconvert", "convert");
    player->sink = gst_element_factory_make("alsasink", "sink");
    g_object_set(player->source, "location", player->file_path, NULL);

    player->pipeline = gst_pipeline_new("pipeline");

    gst_bin_add_many(GST_BIN(player->pipeline),
                     player->source,
                     player->demux,
                     player->decode,
                     player->convert,
                     player->sink,
                     NULL);

    if (gst_element_link(player->source, player->demux) != TRUE) {
        fprintf(stderr, "Error linking source with demuxer.\n");
        exit(1);
    }

    if (gst_element_link_many(player->decode, player->convert, player->sink, NULL) != TRUE) {
        fprintf(stderr, "Error while linking decoder, converter and sink.\n");
        exit(1);
    }

    g_signal_connect(player->demux, "pad-added", G_CALLBACK(pad_added_handler), player);

    bus = gst_element_get_bus(player->pipeline);
    gst_bus_add_signal_watch(bus);
    g_signal_connect(bus, "message::eos", G_CALLBACK(end_of_stream_handler), player);

    player->scale_value_changed_signal_id = g_signal_connect_swapped(player->scale, "value-changed", G_CALLBACK(dtnchat_audio_player_scale_value_changed), player);
}

static void dtnchat_audio_player_dispose(GObject *obj) {
    DTNChatAudioPlayer *player = DTNCHAT_AUDIO_PLAYER(obj);

    if (player->pipeline)
        gst_element_set_state(player->pipeline, GST_STATE_NULL);

    gst_clear_object(&player->pipeline);
}

static void dtnchat_audio_player_finalize(GObject *obj) {
    DTNChatAudioPlayer *player = DTNCHAT_AUDIO_PLAYER(obj);
    g_free(player->file_path);
}

static void dtnchat_audio_player_class_init(DTNChatAudioPlayerClass *class) {
    GParamSpec *prop_file_path;

    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class), "/it/unibo/dtnchat/audio_player.ui");
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatAudioPlayer, play_pause_button);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatAudioPlayer, current_time_label);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatAudioPlayer, total_time_label);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatAudioPlayer, scale);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatAudioPlayer, adjustment);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatAudioPlayer, play_image);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatAudioPlayer, pause_image);

    G_OBJECT_CLASS(class)->set_property = dtnchat_audio_player_set_property;
    G_OBJECT_CLASS(class)->get_property = dtnchat_audio_player_get_property;
    G_OBJECT_CLASS(class)->constructed = dtnchat_audio_player_constructed;
    G_OBJECT_CLASS(class)->dispose = dtnchat_audio_player_dispose;
    G_OBJECT_CLASS(class)->finalize = dtnchat_audio_player_finalize;

    prop_file_path = g_param_spec_string("file-path",
                                         "FilePath",
                                         "Path of the file to play",
                                         NULL,
                                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

    g_object_class_install_property(G_OBJECT_CLASS(class), PROP_FILE_PATH, prop_file_path);

    g_param_spec_unref(prop_file_path);
}

DTNChatAudioPlayer *dtnchat_audio_player_new(const char *file_path) {
    return g_object_new(DTNCHAT_AUDIO_PLAYER_TYPE,
                        "file-path", file_path,
                        NULL);
}

static void pad_added_handler(GstElement *src, GstPad *new_pad, DTNChatAudioPlayer *player) {
    GstPad *sink_pad = gst_element_get_static_pad(player->decode, "sink");
    GstPadLinkReturn ret;
    GstCaps *new_pad_caps = NULL;
    GstStructure *new_pad_struct = NULL;
    const gchar *new_pad_type = NULL;
    gint64 duration;
    char duration_str[13];

    if (gst_pad_is_linked(sink_pad)) {
        gst_object_unref(sink_pad);
        return;
    }

    new_pad_caps = gst_pad_get_current_caps(new_pad);
    new_pad_struct = gst_caps_get_structure(new_pad_caps, 0);
    new_pad_type = gst_structure_get_name(new_pad_struct);
    if (!g_str_has_prefix(new_pad_type, "audio/x-vorbis")) {
        fprintf(stderr, "DTNChatAudioPlayer: pad_added_handler: invalid stream type found: %s\n", new_pad_type);
        show_error_dialog(GTK_WIDGET(player), "Unable to play: invalid stream type in file");
        gst_caps_unref(new_pad_caps);
        gst_object_unref(sink_pad);
        return;
    }

    ret = gst_pad_link(new_pad, sink_pad);
    if (GST_PAD_LINK_FAILED(ret)) {
        fprintf(stderr, "DTNChatAudioPlayer: pad_added_handler: error while linking demuxer with decoder.\n");
        show_error_dialog(GTK_WIDGET(player), "Unable to play: error while linking demuxer with decoder");
        gst_caps_unref(new_pad_caps);
        gst_object_unref(sink_pad);
        return;
    }

    if (!gst_element_query_duration(player->pipeline, GST_FORMAT_TIME, &duration)) {
        fprintf(stderr, "DTNChatAudioPlayer: pad_added_handler: could not query stream duration.\n");
        show_error_dialog(GTK_WIDGET(player), "Unable to play: error while querying stream duration");
        return;
    }

    strcpy(duration_str, " / ");
    nanoseconds_to_string(duration_str + strlen(duration_str), sizeof(duration_str) - strlen(duration_str), duration);
    gtk_adjustment_set_upper(player->adjustment, (double) duration / GST_SECOND);
    gtk_label_set_label(player->total_time_label, duration_str);

    g_timeout_add(150, G_SOURCE_FUNC(refresh_ui), player);
}

static void end_of_stream_handler(GstBus *bus, GstMessage *msg, DTNChatAudioPlayer *player) {
    GstStateChangeReturn ret;

    ret = gst_element_set_state(player->pipeline, GST_STATE_NULL);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "Error while setting pipeline state to NULL\n");
        show_error_dialog(GTK_WIDGET(player), "Unable to stop: error while setting pipeline state to NULL");
        return;
    }
    gtk_button_set_image(GTK_BUTTON(player->play_pause_button), player->play_image);

    gst_element_seek_simple(player->pipeline, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT, 0);
    gtk_adjustment_set_value(player->adjustment, 0);
    gtk_label_set_label(player->current_time_label, "0:00");
}

static gboolean refresh_ui(DTNChatAudioPlayer *player) {
    gint64 current = -1;
    GstStateChangeReturn ret;
    GstState state;
    char duration_str[10];

    if (!player->pipeline)
        return FALSE;

    ret = gst_element_get_state(player->pipeline, &state, NULL, GST_CLOCK_TIME_NONE);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "DTNChatAudioPlayer: refresh_ui: error while obtaining pipeline state.\n");
        show_error_dialog(GTK_WIDGET(player), "Unable to play: error while obtaining pipeline state");
        return FALSE;
    }

    if (state != GST_STATE_PLAYING && state != GST_STATE_PAUSED) {
        return FALSE;
    }

    if (!gst_element_query_position(player->pipeline, GST_FORMAT_TIME, &current)) {
        fprintf(stderr, "DTNChatAudioPlayer: refresh_ui: could not query current playback position.\n");
        show_error_dialog(GTK_WIDGET(player), "Unable to play: error while querying current playback position");
        return FALSE;
    }

    g_signal_handler_block(player->scale, player->scale_value_changed_signal_id);
    gtk_adjustment_set_value(player->adjustment, (double) current / GST_SECOND);
    g_signal_handler_unblock(player->scale, player->scale_value_changed_signal_id);

    nanoseconds_to_string(duration_str, sizeof(duration_str), current);
    gtk_label_set_label(player->current_time_label, duration_str);

    return TRUE;
}

void dtnchat_audio_player_toggle_pause(DTNChatAudioPlayer *player) {
    GstStateChangeReturn ret;
    GstState state;

    ret = gst_element_get_state(player->pipeline, &state, NULL, GST_CLOCK_TIME_NONE);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "DTNChatAudioPlayer: toggle_pause: error while obtaining pipeline state\n");
        show_error_dialog(GTK_WIDGET(player), "Unable to play: error while obtaining pipeline state");
        return;
    }

    if (state == GST_STATE_PLAYING) {
        ret = gst_element_set_state(player->pipeline, GST_STATE_PAUSED);
        if (ret == GST_STATE_CHANGE_FAILURE) {
            fprintf(stderr, "DTNChatAudioPlayer: toggle_pause: error while setting pipeline state to PLAYING\n");
            show_error_dialog(GTK_WIDGET(player), "Unable to pause: error while setting pipeline state to PLAYING");
            return;
        }

        gtk_button_set_image(GTK_BUTTON(player->play_pause_button), player->play_image);
    } else if (state == GST_STATE_PAUSED || state == GST_STATE_NULL) {
        ret = gst_element_set_state(player->pipeline, GST_STATE_PLAYING);
        if (ret == GST_STATE_CHANGE_FAILURE) {
            fprintf(stderr, "DTNChatAudioPlayer: toggle_pause: error while setting pipeline state to PLAYING\n");
            show_error_dialog(GTK_WIDGET(player), "Unable to play: error while setting pipeline state to PLAYING");
            return;
        }

        gtk_button_set_image(GTK_BUTTON(player->play_pause_button), player->pause_image);
    }
}

void dtnchat_audio_player_scale_value_changed(DTNChatAudioPlayer *player) {
    gdouble position;

    position = gtk_adjustment_get_value(player->adjustment);
    gst_element_seek_simple(player->pipeline, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE, position * GST_SECOND);
}

static void nanoseconds_to_string(char *dest, size_t max_len, gint64 nanoseconds) {
    guint64 min;
    guint64 sec;

    min = nanoseconds / (60 * GST_SECOND);
    sec = (nanoseconds / GST_SECOND) % 60;
    snprintf(dest, max_len, "%lu:%02lu", min, sec);
}
