#include <stdio.h>
#include <glib.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h> //CCaini

#include <unified_api.h>//CCaini It replaces all Unified API .h


#include <bundle.h>
#include <chat_types.h>
#include <util.h>

// Macros for converting little-endian 8-byte values to big endian and viceversa (when needed)
#if __BIG_ENDIAN__
# define htonll(x) (x)
# define ntohll(x) (x)
#else
# define htonll(x) (((uint64_t)htonl((x) & 0xFFFFFFFF) << 32) | htonl((x) >> 32))
# define ntohll(x) (((uint64_t)ntohl((x) & 0xFFFFFFFF) << 32) | ntohl((x) >> 32))
#endif

struct bundle_receiving_thread_data {
    MessageCallback callback;
    gpointer user_data;
};

struct bundle_call_callback_data {
    MessageCallback callback;
    gpointer user_data;
    char *eid;
    Message *msg;
};

static int bundle_load_text(al_types_bundle_object *bundle, Message *msg);
static int bundle_load_media(al_types_bundle_object *bundle, Message *msg, const char *eid);
static int bundle_save_media(const al_types_bundle_object *bundle, Message *msg);
static gpointer bundle_receiving_threadfunc(gpointer data);
static gboolean bundle_call_callback(gpointer user_data);

static al_types_bundle_object bundle_out, bundle_in;
static al_socket_registration_descriptor rd;
static al_types_endpoint_id report_to;

static GThread *receiving_thread = NULL;
static GMutex receiving_thread_stop_mutex;
static gboolean receiving_thread_stop = FALSE;

char *bundle_str_error(al_error err) {
    switch (err) {
        case AL_SUCCESS:
            return "success";
        case AL_ERROR:
            return "generic error";
        case AL_WARNING:
            return "generic warning";
        case AL_WARNING_DESTINATION:
            return "missing destination eid";
        case AL_WARNING_TIMEOUT:
            return "timeout";
        case AL_WARNING_RECEPINTER:
            return "interrupted";
        default:
            return "unknown error";
    }
}

// Prints a buffer in hexadecimal format
void bundle_print(char *buf, size_t buf_len) {
    size_t i;
    for (i = 0; i < buf_len; i++) {
        printf("%02x ", buf[i]);
        if (i % 40 == 0) printf("\n");
    }
    if (i % 40 != 1) printf("\n");
}

int bundle_init(int argc, char **argv) {
    al_error err;
    bundle_options_t bundle_options;
    dtn_suite_options_t dtn_suite_options;
    application_options_t residual_options;

    err = al_socket_init();
    if (err) {
        fprintf(stderr, "Bundle: error initializing socket: %s\n", bundle_str_error(err));
        return -1;
    }

    err = al_bundle_options_parse(argc, argv, &bundle_options, &dtn_suite_options, &residual_options);
    if (err) {
        fprintf(stderr, "Bundle: error initializing socket: %s\n", bundle_str_error(err));
        al_socket_destroy();
        return -1;
    }

    al_bundle_options_free(residual_options);

    if (al_utilities_debug_print_debugger_init(dtn_suite_options.debug_level, FALSE, "dtnchat")) {
        fprintf(stderr, "Bundle: error initializing debug print\n");
        al_socket_destroy();
        return -1;
    }

    err = al_socket_register(&rd, APP_DEMUX, APP_SERVICE_NUMBER,
            dtn_suite_options.eid_format_forced, dtn_suite_options.ipn_local, dtn_suite_options.ip, dtn_suite_options.port);
    if (err) {
        fprintf(stderr, "Bundle: error registering socket: %s\n", bundle_str_error(err));
        al_utilities_debug_print_debugger_destroy();
        al_socket_destroy();
        return -1;
    }

    err = al_bundle_create(&bundle_out);
    if (err) {
        fprintf(stderr, "Bundle: error registering socket: %s\n", bundle_str_error(err));
        al_socket_unregister(rd);
        al_utilities_debug_print_debugger_destroy();
        al_socket_destroy();
        return -1;
    }
    err = al_bundle_create(&bundle_in);
    if (err) {
        fprintf(stderr, "Bundle: error registering socket: %s\n", bundle_str_error(err));
        al_bundle_free(&bundle_out);
        al_socket_unregister(rd);
        al_utilities_debug_print_debugger_destroy();
        al_socket_destroy();
        return -1;
    }

    err = al_bundle_options_set(&bundle_out, bundle_options);
    if (err) {
        fprintf(stderr, "Bundle: error registering socket: %s\n", bundle_str_error(err));
        al_bundle_free(&bundle_in);
        al_bundle_free(&bundle_out);
        al_socket_unregister(rd);
        al_utilities_debug_print_debugger_destroy();
        al_socket_destroy();
        return -1;
    }

    strcpy(report_to.uri, "dtn:none");

    return 0;
}

int bundle_send_message(const char *dest_eid, Message *msg) {
    al_types_endpoint_id dest;
    al_error err;

    if (strlen(dest_eid) + 1 > sizeof(dest.uri)) {
        fprintf(stderr, "Bundle: destination EID too large\n");
        return -1;
    }

    strcpy(dest.uri, dest_eid);

    switch (msg->type) {
        case MESSAGE_TYPE_TEXT:
            if (bundle_load_text(&bundle_out, msg) < 0) {
                return -1;
            }
            break;

        case MESSAGE_TYPE_PHOTO:
        case MESSAGE_TYPE_AUDIO:
        case MESSAGE_TYPE_FILE:
            if (bundle_load_media(&bundle_out, msg, dest_eid) < 0) {
                return -1;
            }
            break;

        default:
            fprintf(stderr, "Bundle: unknown message type\n");
            return -1;
    }

    err = al_socket_send(rd, bundle_out, dest, report_to);
    if (err) {
        fprintf(stderr, "Bundle: cannot send bundle: %s\n", bundle_str_error(err));
        return -1;
    }

    return 0;
}

static int bundle_load_text(al_types_bundle_object *bundle, Message *msg) {
    char *buf;
    size_t buf_len;
    al_error err;

    buf_len = 1 + strlen(msg->text);

    buf = malloc(buf_len);

    buf[0] = msg->type;

    memcpy(&buf[1], msg->text, strlen(msg->text));

    err = al_bundle_set_payload_mem(bundle, buf, buf_len);
    if (err) {
        fprintf(stderr, "Bundle: failed setting bundle payload from memory: %s\n", bundle_str_error(err));
        g_free(buf);
        return -1;
    }

    return 0;
}

static int bundle_load_media(al_types_bundle_object *bundle, Message *msg, const char *eid) {
    char *buf;
    size_t buf_len;
    char *text_copy = NULL;
    char *logical_filename = NULL;
    char *file_path;
    int fd;
    off_t file_size;
    size_t buf_position;
    al_error err;

    buf_len = 1; // initial size: 1 byte for message type

    // First we find out how long the buffer needs to be
    switch (msg->type) {
        case MESSAGE_TYPE_PHOTO:
            file_path = get_existing_photo_path(msg->text, eid);
            break;

        case MESSAGE_TYPE_AUDIO:
            file_path = get_existing_audio_path(msg->text, eid);
            break;

        case MESSAGE_TYPE_FILE:
            text_copy = g_strdup(msg->text);
            logical_filename = split_file_string_pair(text_copy);
            if (!logical_filename) {
                fprintf(stderr, "Bundle: invalid file message filename pair\n");
                return -1;
            }
            file_path = get_existing_file_path(text_copy, eid);
            buf_len += strlen(logical_filename) + 1;
            break;

        default:
            fprintf(stderr, "Bundle: invalid message type\n");
            return -1;
    }

    fd = open(file_path, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "Bundle: could not open file %s for reading: ", file_path);
        perror(NULL);
        return -1;
    }

    file_size = lseek(fd, 0, SEEK_END);
    if (file_size < 0) {
        fprintf(stderr, "Bundle: could not seek to end of file %s: ", file_path);
        perror(NULL);
        close(fd);
        return -1;
    }

    buf_len += file_size;

    if (lseek(fd, 0, SEEK_SET) < 0) {
        fprintf(stderr, "Bundle: could not seek to beginning of file %s: ", file_path);
        perror(NULL);
        close(fd);
        return -1;
    }

    // Now allocate the buffer
    buf = malloc(buf_len);

    // Fill with message type and time
    buf[0] = msg->type;

    buf_position = 1;

    // Fill with logical filename if type is file
    if (msg->type == MESSAGE_TYPE_FILE) {
        strcpy(&buf[buf_position], logical_filename);
        buf_position += strlen(logical_filename) + 1;
        g_free(text_copy);
    }

    // Fill with file contents
    if (read(fd, &buf[buf_position], file_size) < 0) {
        fprintf(stderr, "Bundle: could not read file %s: ", file_path);
        perror(NULL);
        close(fd);
        free(buf);
        return -1;
    }

    if (close(fd) < 0) {
        fprintf(stderr, "Bundle: could not close file %s: ", file_path);
        perror(NULL);
        free(buf);
        return -1;
    }

    g_free(file_path);

    err = al_bundle_set_payload_mem(bundle, buf, buf_len);
    if (err) {
        fprintf(stderr, "Bundle: failed setting bundle payload from memory: %s\n", bundle_str_error(err));
        free(buf);
        return -1;
    }

    return 0;
}

void bundle_start_receiving(MessageCallback callback, gpointer user_data) {
    static struct bundle_receiving_thread_data data;

    if (!receiving_thread) {
        data.callback = callback;
        data.user_data = user_data;

        receiving_thread = g_thread_new("receiving-thread", bundle_receiving_threadfunc, &data);
    }
}

static gpointer bundle_receiving_threadfunc(gpointer data) {
    struct bundle_receiving_thread_data *callback = (struct bundle_receiving_thread_data *) data;
    al_error err;
    size_t buf_len;
    char *buf_val;
    char *eid;
    Message *msg;
    gboolean stop;

    while (1) {
        err = al_socket_receive(rd, &bundle_in, BP_PAYLOAD_MEM, 5);
        if (err) {
            if (err != AL_WARNING_RECEPINTER && err != AL_WARNING_TIMEOUT) {
                g_mutex_lock(&receiving_thread_stop_mutex);
                stop = receiving_thread_stop;
                g_mutex_unlock(&receiving_thread_stop_mutex);
                if (stop) break; // This means the error occurred because the socket was closed

                fprintf(stderr, "Bundle: failed receiving bundle: %s\n", bundle_str_error(err));
            }
            continue;
        }

        buf_len = bundle_in.payload->buf.buf_len;
        buf_val = bundle_in.payload->buf.buf_val;

        printf("Bundle: received bundle of length %zu\n", buf_len);

        // 1 byte = message type
        // rest = contents (at least 1 byte)
        if (buf_len < 2) {
            fprintf(stderr, "Bundle: invalid message received:\n");
            bundle_print(buf_val, buf_len);
            continue;
        }

        msg = malloc(sizeof(Message));

        msg->direction = MESSAGE_DIRECTION_IN;
        msg->type = (MessageType) buf_val[0]; // Byte 0 is message type

        if (bundle_in.spec->creation_ts.time) {
            // Get bundle creation time and add 30 year gap to convert from DTN (ms since 2000) to Unix (s since 1970):
            msg->time = bundle_in.spec->creation_ts.time / 1000 + 946684800;
        } else {
            // If creation time is not available use current time
            msg->time = time(NULL);
        }

        switch (msg->type) {
            case MESSAGE_TYPE_TEXT:
                // The rest of the buffer is the message content
                msg->text = malloc(buf_len); // buf_len - 1 + 1 (minus message type, plus null terminator)
                memcpy(msg->text, &buf_val[1], buf_len - 1);
                msg->text[buf_len - 1] = '\0';

                break;

            case MESSAGE_TYPE_PHOTO:
            case MESSAGE_TYPE_AUDIO:
            case MESSAGE_TYPE_FILE:
                if (bundle_save_media(&bundle_in, msg) < 0) {
                    continue;
                }
                break;

            default:
                fprintf(stderr, "Bundle: invalid type for received message\n");
                continue;
        }

        eid = g_strdup(bundle_in.spec->source.uri);

        struct bundle_call_callback_data *call_callback_data = malloc(sizeof(struct bundle_call_callback_data));
        call_callback_data->callback = callback->callback;
        call_callback_data->user_data = callback->user_data;
        call_callback_data->eid = eid;
        call_callback_data->msg = msg;

        GSource *source;
        source = g_idle_source_new();
        g_source_set_callback(source, bundle_call_callback, call_callback_data, NULL);
        g_source_attach(source, g_main_context_default());
        g_source_unref(source);
    }

    return NULL;
}

// msg->text needs to be freed by the caller
static int bundle_save_media(const al_types_bundle_object *bundle, Message *msg) {
    size_t buf_len;
    char *buf_val;
    char *eid;
    char *file_path;
    char *file_content;
    size_t file_content_size;
    int fd;

    buf_len = bundle->payload->buf.buf_len;
    buf_val = bundle->payload->buf.buf_val;
    eid = bundle->spec->source.uri;

    switch (msg->type) {
        case MESSAGE_TYPE_PHOTO:
            if (get_path_for_photo(&file_path, eid) < 0) {
                fprintf(stderr, "Bundle: could not get path for received photo\n");
                return -1;
            }
            break;

        case MESSAGE_TYPE_AUDIO:
            if (get_path_for_audio(&file_path, eid) < 0) {
                fprintf(stderr, "Bundle: could not get path for received audio\n");
                return -1;
            }
            break;

        case MESSAGE_TYPE_FILE:
            if (get_path_for_file(&file_path, eid) < 0) {
                fprintf(stderr, "Bundle: could not get path for received file\n");
                return -1;
            }
            break;

        default:
            fprintf(stderr, "Bundle: invalid type for received message");
            return -1;
    }

    if (msg->type == MESSAGE_TYPE_FILE) {
        int logical_name_length = strnlen(&buf_val[1], buf_len - 1);
        if (logical_name_length == buf_len - 1) {
            fprintf(stderr, "Bundle: invalid message of type file: no null separator between name and content\n");
            return -1;
        }

        char *file_name = get_file_name_from_path(file_path);

        msg->text = malloc(strlen(file_name) + logical_name_length + 2);
        sprintf(msg->text, "%s/%s", file_name, &buf_val[1]);
        file_content = &buf_val[1 + logical_name_length + 1];
        file_content_size = buf_len - (1 + logical_name_length + 1);
    } else {    // MESSAGE_TYPE_PHOTO || MESSAGE_TYPE_AUDIO
        msg->text = g_strdup(get_file_name_from_path(file_path));
        file_content = &buf_val[1];
        file_content_size = buf_len - 1;
    }

    fd = open(file_path, O_WRONLY | O_CREAT, 0644);
    if (fd < 0) {
        fprintf(stderr, "Bundle: could not create file %s: ", file_path);
        perror(NULL);
        free(msg->text);
        return -1;
    }

    if (write(fd, file_content, file_content_size) < 0) {
        fprintf(stderr, "Bundle: could not write to file %s: ", file_path);
        perror(NULL);
        close(fd);
        free(msg->text);
        return -1;
    }

    if (close(fd) < 0) {
        fprintf(stderr, "Bundle: could not close file %s: ", file_path);
        perror(NULL);
        free(msg->text);
        return -1;
    }

    return 0;
}

static gboolean bundle_call_callback(gpointer user_data) {
    struct bundle_call_callback_data *data = (struct bundle_call_callback_data *) user_data;

    data->callback(data->user_data, data->eid, data->msg);

    g_free(data->eid);
    g_free(data->msg->text);
    g_free(data->msg);
    g_free(data);

    return FALSE;
}

void bundle_finalize() {
    al_error err;

    g_mutex_lock(&receiving_thread_stop_mutex);
    receiving_thread_stop = TRUE;
    g_mutex_unlock(&receiving_thread_stop_mutex);

    err = al_bundle_free(&bundle_out);
    if (err)
        fprintf(stderr, "Bundle: could not free bundle_out: %s\n", bundle_str_error(err));

    err = al_bundle_free(&bundle_in);
    if (err)
        fprintf(stderr, "Bundle: could not free bundle_out: %s\n", bundle_str_error(err));

    err = al_socket_unregister(rd);
    if (err)
        fprintf(stderr, "Bundle: could not unregister socket: %s\n", bundle_str_error(err));

    al_socket_destroy();
}
