#include <gtk/gtk.h>
#include <ctype.h>

#include <dtnchat_view.h>
#include <dtnchat_take_photo_dialog.h>
#include <dtnchat_record_audio_dialog.h>
#include <dtnchat_audio_player.h>
#include <dtnchat_file_handler.h>
#include <dtnchat_save_file_button.h>
#include <util.h>
#include <bundle.h>
#include <storage.h>

struct _DTNChatView {
    GtkGrid parent;
    GtkWidget *message_send_box;
    GtkWidget *photo_button;
    GtkWidget *audio_button;
    GtkWidget *file_button;
    GtkWidget *message_entry;
    GtkWidget *send_button;
    GtkWidget *message_view_scrolled_window;
    GtkWidget *message_view_viewport;
    GtkWidget *message_view_listbox;

    char *eid;

    struct tm last_date;
    int last_date_set;
};

G_DEFINE_TYPE(DTNChatView, dtnchat_view, GTK_TYPE_GRID);

#define PROP_CHAT 1

static void dtnchat_view_fill(DTNChatView *view, const Chat *chat);
void dtnchat_view_add_date(DTNChatView *chat_view, struct tm *tp);
static gboolean dtnchat_view_show_image(GtkWidget *event_box, GdkEventButton *event, char *file_path);
static void dtnchat_view_apply_hand_cursor_to_photo(GtkImage *image);

// Scrolls a GtkAdjustment to the bottom completely
static void dtnchat_view_on_listbox_size_allocate(DTNChatView *view, GtkAllocation *alloc) {
    GtkAdjustment *adjustment = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(view->message_view_scrolled_window));
    gtk_adjustment_set_value(adjustment, gtk_adjustment_get_upper(adjustment));
    g_signal_handlers_disconnect_by_func(view->message_view_listbox, dtnchat_view_on_listbox_size_allocate, view);
}

static void dtnchat_view_init(DTNChatView *chat_view) {
    gtk_widget_init_template(GTK_WIDGET(chat_view));

    g_signal_connect(chat_view->message_entry, "map", G_CALLBACK(gtk_widget_grab_focus), NULL);

    chat_view->last_date_set = 0;
}

static void dtnchat_view_set_property(GObject *obj, guint property_id, const GValue *val, GParamSpec *pspec) {
    DTNChatView *view = DTNCHAT_VIEW(obj);

    if (property_id == PROP_CHAT) {
        // Fill the view with the provided messages if there are any
        Chat *chat = g_value_get_pointer(val);
        view->eid = g_strdup(chat->eid);
        if (chat->messages) {
            dtnchat_view_fill(view, chat);
        }
    } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_view_finalize(GObject *obj) {
    DTNChatView *view = DTNCHAT_VIEW(obj);

    g_free(view->eid);
}

static void dtnchat_view_class_init(DTNChatViewClass *class) {
    GParamSpec *prop_chat;

    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class), "/it/unibo/dtnchat/chat_view.ui");
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, message_send_box);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, photo_button);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, audio_button);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, file_button);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, message_entry);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, send_button);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, message_view_scrolled_window);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, message_view_viewport);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatView, message_view_listbox);

    G_OBJECT_CLASS(class)->set_property = dtnchat_view_set_property;
    G_OBJECT_CLASS(class)->finalize = dtnchat_view_finalize;

    prop_chat = g_param_spec_pointer("chat",
                                     "Chat",
                                     "Chat to preload the view with",
                                     G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

    g_object_class_install_property(G_OBJECT_CLASS(class), PROP_CHAT, prop_chat);

    g_param_spec_unref(prop_chat);
}

DTNChatView *dtnchat_view_new(Chat *chat) {
    return g_object_new(DTNCHAT_VIEW_TYPE,
                        "chat", chat,
                        NULL);
}

static void dtnchat_view_fill(DTNChatView *view, const Chat *chat) {
    for (int i = 0; i < chat->n_messages; i++) {
        Message *msg = &chat->messages[i];
        dtnchat_view_add_message(view, msg);
    }
}

void dtnchat_view_add_message(DTNChatView *chat_view, const Message *msg) {
    struct tm *tp;
    char time_label_text[256];
    GtkListBox *listbox;
    GtkWidget *message_container;
    GtkWidget *message_widget;
    GtkWidget *send_info;
    GtkWidget *sender_label;
    GtkWidget *time_label;
    char *file_path = NULL;

    char *start, *end, *text;
    GtkLabel *label;

    GdkPixbuf *image_pixbuf;
    GtkWidget *image;

    char *text_copy;
    char *logical_name;

    DTNChatSaveFileButton *save_file_button;

    listbox = GTK_LIST_BOX(chat_view->message_view_listbox);

    switch (msg->type) {
        case MESSAGE_TYPE_TEXT:
            start = "<span size='11000'>";
            end = "</span>";

            text = malloc(strlen(start) + strlen(msg->text) + strlen(end) + 1);
            strcpy(text, start);
            strcat(text, msg->text);
            strcat(text, end);

            message_widget = gtk_label_new(NULL);
            label = GTK_LABEL(message_widget);
            gtk_label_set_markup(label, text);

            free(text);

            gtk_label_set_xalign(label, msg->direction == MESSAGE_DIRECTION_OUT ? 1 : 0);
            gtk_label_set_line_wrap(label, TRUE);
            gtk_label_set_selectable(label, TRUE);

            break;


        case MESSAGE_TYPE_PHOTO:
            file_path = get_existing_photo_path(msg->text, chat_view->eid);

            message_widget = gtk_event_box_new();

            image_pixbuf = gdk_pixbuf_new_from_file_at_size(file_path, 300, 200, NULL);
            image = gtk_image_new_from_pixbuf(image_pixbuf);
            g_object_unref(image_pixbuf);

            gtk_container_add(GTK_CONTAINER(message_widget), image);

            g_signal_connect(image, "realize", G_CALLBACK(dtnchat_view_apply_hand_cursor_to_photo), NULL);
            g_signal_connect(message_widget, "button-press-event", G_CALLBACK(dtnchat_view_show_image), file_path);

            break;


        case MESSAGE_TYPE_AUDIO:
            file_path = get_existing_audio_path(msg->text, chat_view->eid);
            message_widget = GTK_WIDGET(dtnchat_audio_player_new(file_path));

            break;


        case MESSAGE_TYPE_FILE:
            text_copy = g_strdup(msg->text);

            logical_name = split_file_string_pair(text_copy);
            if (!logical_name) {
                fprintf(stderr, "dtnchat_view_add_message: invalid file message filename pair\n");
                return;
            }

            file_path = get_existing_file_path(text_copy, chat_view->eid);

            message_widget = GTK_WIDGET(dtnchat_file_handler_new(file_path, logical_name));

            free(file_path);
            free(text_copy);

            break;


        default:
            fprintf(stderr, "%s: invalid message type\n", __func__);
            return;
    }

    strcpy(time_label_text, "<span size='8000'>");
    tp = localtime(&msg->time);
    strftime(time_label_text + strlen(time_label_text), sizeof(time_label_text) - strlen(time_label_text), "%H:%M", tp);
    strcat(time_label_text, "</span>");

    if (!chat_view->last_date_set || chat_view->last_date.tm_mday != tp->tm_mday) {
        dtnchat_view_add_date(chat_view, tp);
    }
    memcpy(&chat_view->last_date, tp, sizeof(struct tm));
    chat_view->last_date_set = 1;

    sender_label = gtk_image_new_from_icon_name(msg->direction == MESSAGE_DIRECTION_OUT ? "go-up" : "go-down", GTK_ICON_SIZE_LARGE_TOOLBAR);

    time_label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(time_label), time_label_text);

    message_container = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 20);
    gtk_widget_set_halign(message_container, msg->direction == MESSAGE_DIRECTION_OUT ? GTK_ALIGN_END : GTK_ALIGN_START);
    gtk_widget_set_margin_top(message_container, 2);
    gtk_widget_set_margin_bottom(message_container, 2);
    gtk_widget_set_margin_start(message_container, 5);
    gtk_widget_set_margin_end(message_container, 5);

    if (msg->type == MESSAGE_TYPE_PHOTO || msg->type == MESSAGE_TYPE_AUDIO) {
        char *logical_name = (msg->type == MESSAGE_TYPE_PHOTO) ? "Untitled.jpeg" : "Untitled.ogg";

        save_file_button = dtnchat_save_file_button_new(file_path, logical_name, TRUE);
        gtk_widget_set_valign(GTK_WIDGET(save_file_button), GTK_ALIGN_CENTER);

        if (msg->type == MESSAGE_TYPE_AUDIO) {
            free(file_path);
        }

        gtk_container_add(GTK_CONTAINER(message_container), GTK_WIDGET(save_file_button));
        gtk_box_reorder_child(GTK_BOX(message_container), GTK_WIDGET(save_file_button), 1);
    }

    send_info = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(send_info), sender_label, FALSE, FALSE, 0);
    gtk_box_pack_end(GTK_BOX(send_info), time_label, FALSE, FALSE, 0);

    gtk_container_add(GTK_CONTAINER(message_container), send_info);
    gtk_box_reorder_child(GTK_BOX(message_container), send_info, msg->direction == MESSAGE_DIRECTION_OUT ? 2 : 0);
    gtk_widget_set_valign(send_info, GTK_ALIGN_CENTER);

    gtk_container_add(GTK_CONTAINER(message_container), message_widget);
    gtk_box_reorder_child(GTK_BOX(message_container), message_widget, msg->direction == MESSAGE_DIRECTION_OUT ? 0 : 2);
    gtk_widget_set_valign(message_widget, GTK_ALIGN_CENTER);

    gtk_container_add(GTK_CONTAINER(listbox), message_container);
    gtk_widget_show_all(message_container);

    g_signal_connect_swapped(listbox, "size-allocate", G_CALLBACK(dtnchat_view_on_listbox_size_allocate), chat_view);
}

void dtnchat_view_add_date(DTNChatView *chat_view, struct tm *tp) {
    GtkContainer *listbox;
    GtkWidget *label;
    char date_buf[11];

    listbox = GTK_CONTAINER(chat_view->message_view_listbox);

    strftime(date_buf, sizeof(date_buf), "%x", tp);

    label = gtk_label_new(date_buf);
    gtk_widget_set_halign(label, GTK_ALIGN_CENTER);
    gtk_widget_set_margin_top(label, 5);
    gtk_widget_set_margin_bottom(label, 5);
    gtk_container_add(listbox, label);
    gtk_widget_show(label);
}

// Checks if a string is empty or only contains space characters
static gboolean str_is_blank(const char *str) {
    for (; *str; str++)
        if (!isspace(*str))
            return FALSE;
    return TRUE;
}

// Sends a message
void dtnchat_view_send_text_message(DTNChatView *view) {
    Message msg;
    GtkEntry *entry;
    const gchar *internal_text;

    entry = GTK_ENTRY(view->message_entry);

    internal_text = gtk_entry_get_text(entry);
    if (str_is_blank(internal_text)) {
        gtk_entry_set_text(entry, "");
        return;
    }

    msg.direction = MESSAGE_DIRECTION_OUT;
    msg.type = MESSAGE_TYPE_TEXT;
    msg.time = time(NULL);
    msg.text = g_strdup(internal_text);

    dtnchat_view_add_message(view, &msg);
    if (bundle_send_message(view->eid, &msg) < 0)
        return;
    storage_add_message(view->eid, &msg);

    g_free(msg.text);

    printf("Entry submitted: %s\n", internal_text);
    gtk_entry_set_text(entry, "");
}

void dtnchat_view_send_photo(DTNChatView *view, char *file_path) {
    Message msg;

    msg.direction = MESSAGE_DIRECTION_OUT;
    msg.type = MESSAGE_TYPE_PHOTO;
    msg.time = time(NULL);
    msg.text = get_file_name_from_path(file_path);

    dtnchat_view_add_message(view, &msg);
    if (bundle_send_message(view->eid, &msg) < 0)
        return;
    storage_add_message(view->eid, &msg);
}

static void dtnchat_view_apply_hand_cursor_to_photo(GtkImage *image) {
    gdk_window_set_cursor(gtk_widget_get_window(GTK_WIDGET(image)),
                          gdk_cursor_new_for_display(gdk_display_get_default(),
                          GDK_HAND2));
}

void dtnchat_view_on_photo_button_clicked(DTNChatView *view) {
    DTNChatTakePhotoDialog *dialog;
    char *file_path;

    if (get_path_for_photo(&file_path, view->eid) < 0)
        return;

    dialog = dtnchat_take_photo_dialog_new(file_path);
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(gtk_widget_get_ancestor(GTK_WIDGET(view), GTK_TYPE_WINDOW)));
    g_signal_connect_swapped(dialog, "done", G_CALLBACK(dtnchat_view_send_photo), view);
    gtk_widget_show_all(GTK_WIDGET(dialog));
}

static gboolean dtnchat_view_show_image(GtkWidget *event_box, GdkEventButton *event, char *file_path) {
    if (event->button == 1) {
        GtkWindow *dialog;
        GtkWidget *image;

        dialog = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
        gtk_window_set_title(dialog, file_path);
        gtk_window_set_modal(dialog, TRUE);
        gtk_window_set_destroy_with_parent(dialog, TRUE);
        gtk_window_set_transient_for(dialog, GTK_WINDOW(gtk_widget_get_ancestor(event_box, GTK_TYPE_WINDOW)));
        gtk_window_set_resizable(dialog, FALSE);

        image = gtk_image_new_from_file(file_path);
        gtk_container_add(GTK_CONTAINER(dialog), image);
        gtk_widget_show_all(GTK_WIDGET(dialog));
    }

    return TRUE;
}

void dtnchat_view_send_audio(DTNChatView *view, gint response_id, DTNChatRecordAudioDialog *dialog) {
    if (response_id == GTK_RESPONSE_OK) {
        Message msg;

        msg.direction = MESSAGE_DIRECTION_OUT;
        msg.type = MESSAGE_TYPE_AUDIO;
        msg.time = time(NULL);
        msg.text = get_file_name_from_path(dtnchat_record_audio_dialog_get_file_path(dialog));

        dtnchat_view_add_message(view, &msg);
        if (bundle_send_message(view->eid, &msg) < 0)
            return;
        storage_add_message(view->eid, &msg);
    }
}

void dtnchat_view_on_audio_button_clicked(DTNChatView *view) {
    DTNChatRecordAudioDialog *dialog;
    char *file_path;

    if (get_path_for_audio(&file_path, view->eid) < 0)
        return;

    dialog = dtnchat_record_audio_dialog_new(file_path);
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(gtk_widget_get_ancestor(GTK_WIDGET(view), GTK_TYPE_WINDOW)));
    g_signal_connect_swapped(dialog, "response", G_CALLBACK(dtnchat_view_send_audio), view);
    gtk_widget_show_all(GTK_WIDGET(dialog));
}

void dtnchat_view_on_file_button_clicked(DTNChatView *view) {
    GtkFileChooserNative *file_chooser;
    gint response;

    // First we show a file selection dialog

    file_chooser = gtk_file_chooser_native_new("Select file",
                                               GTK_WINDOW(gtk_widget_get_ancestor(GTK_WIDGET(view), GTK_TYPE_WINDOW)),
                                               GTK_FILE_CHOOSER_ACTION_OPEN,
                                               "_Open",
                                               "_Cancel");
    response = gtk_native_dialog_run(GTK_NATIVE_DIALOG(file_chooser));

    if (response == GTK_RESPONSE_ACCEPT) {
        Message msg;
        char *src_path, *dest_path;
        char *logical_filename, *physical_filename;
        GFile *src, *dest;
        char confirm_message[512];
        GtkWidget *confirm_dialog, *content_area, *confirm_message_label;

        // Now we show a confirmation dialog to make sure the user really wants to send the file

        src_path = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));

        confirm_dialog = gtk_dialog_new_with_buttons("Send file",
                                                     GTK_WINDOW(gtk_widget_get_ancestor(GTK_WIDGET(view), GTK_TYPE_WINDOW)),
                                                     GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                                     "Ok",
                                                     GTK_RESPONSE_ACCEPT,
                                                     "Cancel",
                                                     GTK_RESPONSE_REJECT,
                                                     NULL);
        content_area = gtk_dialog_get_content_area(GTK_DIALOG(confirm_dialog));
        sprintf(confirm_message, "Send file '%s'?", get_file_name_from_path(src_path));
        confirm_message_label = gtk_label_new(confirm_message);
        g_object_set(confirm_message_label, "margin", 20, NULL);
        gtk_container_add(GTK_CONTAINER(content_area), confirm_message_label);
        gtk_widget_show(confirm_message_label);

        response = gtk_dialog_run(GTK_DIALOG(confirm_dialog));
        gtk_widget_destroy(confirm_dialog);

        if (response == GTK_RESPONSE_ACCEPT) {
            if (get_path_for_file(&dest_path, view->eid) < 0) {
                fprintf(stderr, "%s: Could not get new path for file\n", __func__);
                return;
            }

            src = g_file_new_for_path(src_path);
            dest = g_file_new_for_path(dest_path);
            g_file_copy(src, dest, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, NULL);

            g_object_unref(src);
            g_object_unref(dest);

            logical_filename = get_file_name_from_path(src_path);
            physical_filename = get_file_name_from_path(dest_path);

            msg.direction = MESSAGE_DIRECTION_OUT;
            msg.type = MESSAGE_TYPE_FILE;
            msg.time = time(NULL);
            msg.text = malloc(strlen(logical_filename) + strlen(physical_filename) + 2);
            sprintf(msg.text, "%s/%s", physical_filename, logical_filename);


            dtnchat_view_add_message(view, &msg);
            if (bundle_send_message(view->eid, &msg) == 0) {
                storage_add_message(view->eid, &msg);
            }

            free(src_path);
            free(msg.text);
        }
    }

    g_object_unref(file_chooser);
}
