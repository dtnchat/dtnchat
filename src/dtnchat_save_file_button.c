#include <gtk/gtk.h>

#include <dtnchat_save_file_button.h>

struct _DTNChatSaveFileButton {
    GtkButton parent;
    GtkWidget *image;

    char *file_path;
    char *logical_filename;
    gboolean icon_only;
};

G_DEFINE_TYPE(DTNChatSaveFileButton, dtnchat_save_file_button, GTK_TYPE_BUTTON);

typedef enum {
    PROP_FILE_PATH = 1,
    PROP_LOGICAL_FILENAME,
    PROP_ICON_ONLY,
    N_PROP
} DTNCHAT_SAVE_FILE_BUTTON_PROP;

static void dtnchat_save_file_button_init(DTNChatSaveFileButton *button) {
    button->image = gtk_image_new_from_icon_name("document-save", GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image(GTK_BUTTON(button), button->image);
    gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Save as...");
}

static void dtnchat_save_file_button_set_property(GObject *obj, guint property_id, const GValue *val, GParamSpec *pspec) {
    DTNChatSaveFileButton *button = DTNCHAT_SAVE_FILE_BUTTON(obj);

    switch (property_id) {
        case PROP_FILE_PATH:
            g_free(button->file_path);
            button->file_path = g_value_dup_string(val);
            break;

        case PROP_LOGICAL_FILENAME:
            g_free(button->logical_filename);
            button->logical_filename = g_value_dup_string(val);
            break;

        case PROP_ICON_ONLY:
            button->icon_only = g_value_get_boolean(val);
            if (button->icon_only) {
                gtk_button_set_label(GTK_BUTTON(button), NULL);
                gtk_button_set_always_show_image(GTK_BUTTON(button), TRUE);
            } else {
                gtk_button_set_label(GTK_BUTTON(button), "Save as...");
                gtk_button_set_always_show_image(GTK_BUTTON(button), FALSE);
            }
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_save_file_button_get_property(GObject *obj, guint property_id, GValue *val, GParamSpec *pspec) {
    DTNChatSaveFileButton *button = DTNCHAT_SAVE_FILE_BUTTON(obj);

    switch (property_id) {
        case PROP_FILE_PATH:
            g_value_set_string(val, button->file_path);
            break;

        case PROP_LOGICAL_FILENAME:
            g_value_set_string(val, button->logical_filename);
            break;

        case PROP_ICON_ONLY:
            g_value_set_boolean(val, button->icon_only);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_save_file_button_clicked(GtkButton *obj) {
    DTNChatSaveFileButton *button = DTNCHAT_SAVE_FILE_BUTTON(obj);
    GtkWindow *window;
    GtkFileChooserNative *file_chooser;
    gint response;

    window = GTK_WINDOW(gtk_widget_get_ancestor(GTK_WIDGET(button), GTK_TYPE_WINDOW));

    file_chooser = gtk_file_chooser_native_new("Select location",
                                               window,
                                               GTK_FILE_CHOOSER_ACTION_SAVE,
                                               "Save",
                                               "Cancel");

    gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(file_chooser), TRUE);
    gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(file_chooser), button->logical_filename);

    response = gtk_native_dialog_run(GTK_NATIVE_DIALOG(file_chooser));

    if (response == GTK_RESPONSE_ACCEPT) {
        char *dest_path;
        GFile *src, *dest;

        dest_path = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));

        src = g_file_new_for_path(button->file_path);
        dest = g_file_new_for_path(dest_path);

        g_file_copy(src, dest, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, NULL);

        g_object_unref(src);
        g_object_unref(dest);
        g_free(dest_path);
    }

    g_object_unref(file_chooser);
}

static void dtnchat_save_file_button_class_init(DTNChatSaveFileButtonClass *class) {
    GParamSpec *props[N_PROP] = { NULL, };

    G_OBJECT_CLASS(class)->set_property = dtnchat_save_file_button_set_property;
    G_OBJECT_CLASS(class)->get_property = dtnchat_save_file_button_get_property;
    GTK_BUTTON_CLASS(class)->clicked = dtnchat_save_file_button_clicked;

    props[PROP_FILE_PATH] = g_param_spec_string("file-path",
                                                "FilePath",
                                                "Path of the file to save",
                                                NULL,
                                                G_PARAM_READWRITE);

    props[PROP_LOGICAL_FILENAME] = g_param_spec_string("logical-filename",
                                                       "LogicalFilename",
                                                       "Filename to show to the user",
                                                       NULL,
                                                       G_PARAM_READWRITE);

    props[PROP_ICON_ONLY] = g_param_spec_boolean("icon-only",
                                                 "IconOnly",
                                                 "Whether to show only show an icon on the button",
                                                 FALSE,
                                                 G_PARAM_CONSTRUCT | G_PARAM_READWRITE);

    g_object_class_install_properties(G_OBJECT_CLASS(class), N_PROP, props);

    for (int i = 1; i < N_PROP; i++) {
        g_param_spec_unref(props[i]);
    }
}

DTNChatSaveFileButton *dtnchat_save_file_button_new(char *file_path, char *logical_filename, gboolean icon_only) {
    DTNChatSaveFileButton *button = g_object_new(DTNCHAT_SAVE_FILE_BUTTON_TYPE,
                                                 "file-path", file_path,
                                                 "logical-filename", logical_filename,
                                                 "icon-only", icon_only,
                                                 NULL);

    return button;
}
