#include <gtk/gtk.h>
#include <stdio.h>

#include <dtnchat_app.h>
#include <dtnchat_window.h>
#include <dtnchat_view.h>
#include <dtnchat_recipient_dialog.h>
#include <bundle.h>
#include <storage.h>
#include <util.h>

struct _DTNChatWindow {
    GtkApplicationWindow parent;
    GtkWidget *paned;
    GtkWidget *stack;
    GtkWidget *stack_sidebar;
    GtkWidget *header;
    GtkWidget *new_chat_button;
    GtkWidget *delete_chat_button;
};

G_DEFINE_TYPE(DTNChatWindow, dtnchat_window, GTK_TYPE_APPLICATION_WINDOW);

void dtnchat_window_add_view(DTNChatWindow *window, DTNChatView *view, const char *eid);
void dtnchat_window_message_received_callback(gpointer data, char *eid, Message *msg);

static void dtnchat_window_init(DTNChatWindow *win) {
    gtk_widget_init_template(GTK_WIDGET(win));
    gtk_window_set_icon_name(GTK_WINDOW(win), "it.unibo.dtnchat");
}

static void dtnchat_window_realize(GtkWidget *obj) {
    DTNChatWindow *window = DTNCHAT_WINDOW(obj);
    Chat *chats;
    int n_chats;

    // Chain up
    GTK_WIDGET_CLASS(dtnchat_window_parent_class)->realize(obj);

    // Create the views for already existing chats (from the file system)

    n_chats = storage_load_chats(&chats);
    if (n_chats < 0) {
        exit(1);
    }

    for (int i = 0; i < n_chats; i++) {
        DTNChatView *view = dtnchat_view_new(&chats[i]);
        dtnchat_window_add_view(window, view, chats[i].eid);
    }

    storage_free_chats(chats, n_chats);

    // Start receiving new messages
    bundle_start_receiving(dtnchat_window_message_received_callback, window);
}

static void dtnchat_window_class_init(DTNChatWindowClass *class) {
    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class), "/it/unibo/dtnchat/window.ui");
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatWindow, paned);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatWindow, stack);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatWindow, stack_sidebar);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatWindow, header);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatWindow, new_chat_button);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatWindow, delete_chat_button);

    GTK_WIDGET_CLASS(class)->realize = dtnchat_window_realize;
}

DTNChatWindow *dtnchat_window_new(DTNChatApp *app) {
    return g_object_new(DTNCHAT_WINDOW_TYPE, "application", app, NULL);
}

void dtnchat_window_update_delete_chat_button_sensitivity(DTNChatWindow *window) {
    GList *children = gtk_container_get_children(GTK_CONTAINER(window->stack));
    int number_of_children = g_list_length(children);
    g_list_free(children);
    gtk_widget_set_sensitive(window->delete_chat_button, number_of_children ? TRUE : FALSE);
}

void dtnchat_window_add_view(DTNChatWindow *window, DTNChatView *view, const char *eid) {
    GtkStack *stack = GTK_STACK(window->stack);
    gtk_stack_add_titled(stack, GTK_WIDGET(view), eid, eid);
    gtk_stack_set_visible_child(stack, GTK_WIDGET(view));
    dtnchat_window_update_delete_chat_button_sensitivity(window);
}

void dtnchat_window_message_received_callback(gpointer data, char *eid, Message *msg) {
    DTNChatWindow *win = DTNCHAT_WINDOW(data);
    GtkStack *stack = GTK_STACK(win->stack);
    GtkWidget *view_widget = gtk_stack_get_child_by_name(stack, eid);
    DTNChatView *view;

    if (view_widget) {
        gtk_stack_set_visible_child(stack, view_widget);
        view = DTNCHAT_VIEW(view_widget);
    } else { // Chat with this recipient doesn't exist yet; create a view for it
        Chat chat;

        chat.eid = g_strdup(eid);
        chat.messages = NULL;
        chat.n_messages = 0;

        view = dtnchat_view_new(&chat);
        dtnchat_window_add_view(win, view, chat.eid);

        g_free(chat.eid);
    }

    dtnchat_view_add_message(view, msg);
    if (!storage_add_message(eid, msg)) {
        return;
    }
}

// A struct that holds the user data for the on_recipient_dialog_response function
struct on_recipient_dialog_response_data {
    DTNChatWindow *win;
    GtkEntry *entry;
};

// Callback for the "response" signal of the new chat dialog (i.e. for when the dialog is closed)
void on_recipient_dialog_response(GtkDialog *dialog, gint response_id, struct on_recipient_dialog_response_data *data) {
    if (response_id == GTK_RESPONSE_OK) {
        DTNChatView *view;
        Chat chat;
        const gchar *entry_text;

        entry_text = gtk_entry_get_text(data->entry);

        if (gtk_stack_get_child_by_name(GTK_STACK(data->win->stack), entry_text)) {
            show_error_dialog(GTK_WIDGET(data->win->stack), "A chat with that destination EID already exists!");
            return;
        }

        chat.eid = g_strdup(entry_text);
        chat.messages = NULL;
        chat.n_messages = 0;

        if (!storage_add_chat(chat.eid))
            return;

        view = dtnchat_view_new(&chat);

        dtnchat_window_add_view(data->win, view, chat.eid);
        g_free(chat.eid);
    }
}

// Callback for when the user clicks on the new chat button
void dtnchat_window_on_new_chat_button_clicked(DTNChatWindow *window) {
    DTNChatRecipientDialog *dialog;
    static struct on_recipient_dialog_response_data signal_data;

    dialog = dtnchat_recipient_dialog_new();
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(window));

    signal_data.win = window;
    signal_data.entry = dtnchat_recipient_dialog_get_entry(dialog);

    g_signal_connect(dialog, "response", G_CALLBACK(on_recipient_dialog_response), &signal_data);
    gtk_widget_show_all(GTK_WIDGET(dialog));
}

void dtnchat_window_on_delete_chat_dialog_response(DTNChatWindow *window, gint response_id, GtkWidget *dialog) {
    if (response_id == GTK_RESPONSE_OK) {
        GtkWidget *view;

        if (!storage_remove_chat(gtk_stack_get_visible_child_name(GTK_STACK(window->stack)))) {
            return;
        }
        view = gtk_stack_get_visible_child(GTK_STACK(window->stack));
        gtk_widget_destroy(view);
        dtnchat_window_update_delete_chat_button_sensitivity(window);
    }
    gtk_widget_destroy(dialog);
}

void dtnchat_window_on_delete_chat_button_clicked(DTNChatWindow *window) {
    GtkWidget *confirm_dialog;
    GtkWidget *content_area;
    GtkWidget *confirm_message_label;
    GtkWidget *button_ok;
    GtkWidget *button_cancel;

    confirm_dialog = gtk_dialog_new();
    gtk_window_set_title(GTK_WINDOW(confirm_dialog), "Delete chat");
    gtk_window_set_resizable(GTK_WINDOW(confirm_dialog), FALSE);
    gtk_window_set_modal(GTK_WINDOW(confirm_dialog), TRUE);
    gtk_window_set_destroy_with_parent(GTK_WINDOW(confirm_dialog), TRUE);
    gtk_window_set_transient_for(GTK_WINDOW(confirm_dialog), GTK_WINDOW(window));

    button_cancel = gtk_button_new_with_label("Cancel");
    gtk_dialog_add_action_widget(GTK_DIALOG(confirm_dialog), button_cancel, GTK_RESPONSE_CANCEL);

    button_ok = gtk_button_new_with_label("Ok");
    gtk_style_context_add_class(gtk_widget_get_style_context(button_ok), "destructive-action");
    gtk_dialog_add_action_widget(GTK_DIALOG(confirm_dialog), button_ok, GTK_RESPONSE_OK);

    content_area = gtk_dialog_get_content_area(GTK_DIALOG(confirm_dialog));
    confirm_message_label = gtk_label_new("Are you sure you want to delete this chat?");
    g_object_set(confirm_message_label, "margin", 20, NULL);
    gtk_container_add(GTK_CONTAINER(content_area), confirm_message_label);

    g_signal_connect_swapped(confirm_dialog, "response", G_CALLBACK(dtnchat_window_on_delete_chat_dialog_response), window);
    gtk_widget_show_all(confirm_dialog);
}
