#include <gtk/gtk.h>
#include <cheese/cheese-gtk.h>
#include <cheese/cheese-widget.h>
#include <cheese/cheese-camera.h>
#include <stdio.h>

#include <util.h>
#include <dtnchat_take_photo_dialog.h>

static guint DTNCHAT_TAKE_PHOTO_DIALOG_SIGNAL_DONE;

struct _DTNChatTakePhotoDialog {
    GtkWindow parent;
    GtkWidget *box;
    GtkWidget *button_box;
    GtkWidget *cheese_widget;
    GtkWidget *take_photo_button;
    GtkWidget *preview_image;
    GtkWidget *send_button;
    GtkWidget *cancel_button;

    char *file_path;
};

G_DEFINE_TYPE(DTNChatTakePhotoDialog, dtnchat_take_photo_dialog, GTK_TYPE_WINDOW);

#define PROP_FILE_PATH 1

static void dtnchat_take_photo_dialog_init(DTNChatTakePhotoDialog *dialog) {
    gtk_widget_init_template(GTK_WIDGET(dialog));

    dialog->cheese_widget = cheese_widget_new();
    gtk_box_pack_start(GTK_BOX(dialog->box), dialog->cheese_widget, TRUE, TRUE, 0);
    gtk_widget_show_all(dialog->cheese_widget);
}

static void dtnchat_take_photo_dialog_set_property(GObject *obj, guint property_id, const GValue *val, GParamSpec *pspec) {
    DTNChatTakePhotoDialog *dialog = DTNCHAT_TAKE_PHOTO_DIALOG(obj);

    if (property_id == PROP_FILE_PATH) {
        g_free(dialog->file_path);
        dialog->file_path = g_value_dup_string(val);
    } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_take_photo_dialog_get_property(GObject *obj, guint property_id, GValue *val, GParamSpec *pspec) {
    DTNChatTakePhotoDialog *dialog = DTNCHAT_TAKE_PHOTO_DIALOG(obj);

    if (property_id == PROP_FILE_PATH) {
        g_value_set_string(val, dialog->file_path);
    } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_take_photo_dialog_finalize(GObject *obj) {
    DTNChatTakePhotoDialog *dialog = DTNCHAT_TAKE_PHOTO_DIALOG(obj);
    g_free(dialog->file_path);
}

static void dtnchat_take_photo_dialog_class_init(DTNChatTakePhotoDialogClass *class) {
    GParamSpec *prop_file_path;

    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class), "/it/unibo/dtnchat/take_photo_dialog.ui");
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatTakePhotoDialog, box);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatTakePhotoDialog, button_box);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatTakePhotoDialog, take_photo_button);

    G_OBJECT_CLASS(class)->set_property = dtnchat_take_photo_dialog_set_property;
    G_OBJECT_CLASS(class)->get_property = dtnchat_take_photo_dialog_get_property;
    G_OBJECT_CLASS(class)->finalize = dtnchat_take_photo_dialog_finalize;

    DTNCHAT_TAKE_PHOTO_DIALOG_SIGNAL_DONE = g_signal_new(
            "done",
            DTNCHAT_TAKE_PHOTO_DIALOG_TYPE,
            G_SIGNAL_RUN_FIRST,
            0,
            NULL,
            NULL,
            NULL,
            G_TYPE_NONE,
            1,
            G_TYPE_STRING
    );

    prop_file_path = g_param_spec_string("file-path",
                                         "FilePath",
                                         "Path of the file to save the photo to",
                                         NULL,
                                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

    g_object_class_install_property(G_OBJECT_CLASS(class), PROP_FILE_PATH, prop_file_path);

    g_param_spec_unref(prop_file_path);
}

DTNChatTakePhotoDialog *dtnchat_take_photo_dialog_new(char *file_path) {
    return g_object_new(DTNCHAT_TAKE_PHOTO_DIALOG_TYPE,
                        "file-path", file_path,
                         NULL);
}

struct dtnchat_take_photo_dialog_on_photo_saved_data {
    DTNChatTakePhotoDialog *dialog;
    char *file_path;
};

void dtnchat_take_photo_dialog_send(struct dtnchat_take_photo_dialog_on_photo_saved_data *data) {
    g_signal_emit(data->dialog, DTNCHAT_TAKE_PHOTO_DIALOG_SIGNAL_DONE, 0, data->file_path);
    gtk_widget_destroy(GTK_WIDGET(data->dialog));
}

void dtnchat_take_photo_dialog_cancel(struct dtnchat_take_photo_dialog_on_photo_saved_data *data) {
    if (unlink(data->file_path) < 0) {
        fprintf(stderr, "%s: unable to unlink file %s: ", __func__, data->file_path);
        perror(NULL);
    }
    gtk_widget_destroy(GTK_WIDGET(data->dialog));
}

void dtnchat_take_photo_dialog_on_photo_saved(struct dtnchat_take_photo_dialog_on_photo_saved_data *data) {
    DTNChatTakePhotoDialog *dialog = data->dialog;
    GdkPixbuf *image_pixbuf;
    GtkAllocation cheese_widget_alloc;

    // We need to get the width and height of the CheeseWidget to make a GtkImage with the same size
    gtk_widget_get_allocation(dialog->cheese_widget, &cheese_widget_alloc);

    gtk_widget_destroy(dialog->take_photo_button);
    gtk_widget_destroy(dialog->cheese_widget);

    image_pixbuf = gdk_pixbuf_new_from_file_at_size(data->file_path,
                                                    cheese_widget_alloc.width,
                                                    cheese_widget_alloc.height,
                                                    NULL);
    dialog->preview_image = gtk_image_new_from_pixbuf(image_pixbuf);
    g_object_unref(image_pixbuf);

    gtk_box_pack_start(GTK_BOX(dialog->box), dialog->preview_image, TRUE, TRUE, 0);

    dialog->send_button = gtk_button_new_with_label("Send");
    g_signal_connect_swapped(dialog->send_button, "clicked", G_CALLBACK(dtnchat_take_photo_dialog_send), data);

    dialog->cancel_button = gtk_button_new_with_label("Cancel");
    g_signal_connect_swapped(dialog->cancel_button, "clicked", G_CALLBACK(dtnchat_take_photo_dialog_cancel), data);

    gtk_box_pack_start(GTK_BOX(dialog->button_box), dialog->cancel_button, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(dialog->button_box), dialog->send_button, TRUE, TRUE, 0);

    gtk_widget_show_all(dialog->box);
}

GObject *cheese_widget_get_camera(CheeseWidget *widget);

void dtnchat_take_photo_dialog_take(DTNChatTakePhotoDialog *dialog) {
    CheeseCamera *cam;

    cam = CHEESE_CAMERA(cheese_widget_get_camera(CHEESE_WIDGET(dialog->cheese_widget)));

    if (!cheese_camera_take_photo(cam, dialog->file_path)) {
        fprintf(stderr, "%s: error taking photo\n", __func__);
        return;
    }

    static struct dtnchat_take_photo_dialog_on_photo_saved_data data;
    data.dialog = dialog;
    data.file_path = dialog->file_path;

    g_signal_connect_swapped(cam, "photo-saved", G_CALLBACK(dtnchat_take_photo_dialog_on_photo_saved), &data);
}
