#include <util.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#include <gtk/gtk.h>

#define MAX_FILENAME_LENGTH 256
#define MAX_PATH_LENGTH 4096

// The returned value must be freed by the caller
char *escape_text(const char *text, const char *escape_chars) {
    size_t i, j, k;
    size_t size;
    int found;
    char *result;

    size = 0;
    for (i = 0; text[i]; i++) {
        if (text[i] == '%') {
            found = 1;
        } else {
            found = 0;
            for (k = 0; escape_chars[k]; k++) {
                if (text[i] == escape_chars[k]) {
                    found = 1;
                    break;
                }
            }
        }
        if (found)
            size += 3;
        else
            size++;
    }

    result = malloc((size + 1) * sizeof(char));

    for (i = 0, j = 0; text[i]; i++) {
        if (text[i] == '%') {
            found = 1;
        } else {
            found = 0;
            for (k = 0; escape_chars[k]; k++) {
                if (text[i] == escape_chars[k]) {
                    found = 1;
                    break;
                }
            }
        }
        if (found) {
            sprintf(&result[j], "%%%02X", text[i]);
            j += 3;
        } else {
            result[j] = text[i];
            j++;
        }
    }

    result[j] = '\0';
    return result;
}

// The returned value must be freed by the caller
char *unescape_text(const char *text) {
    size_t i, j;
    size_t size;
    char buf[3];
    unsigned int current_char;
    char *result;

    size = 0;
    for (i = 0; text[i]; i++) {
        if (text[i] == '%') {
            i += 2;
        }
        size++;
    }

    result = malloc((size + 1) * sizeof(char));
    buf[2] = '\0';

    for (i = 0, j = 0; text[i]; i++, j++) {
        if (text[i] == '%') {
            memcpy(buf, &text[i + 1], 2);
            sscanf(buf, "%02X", &current_char);
            result[j] = current_char;
            i += 2;
        } else {
            result[j] = text[i];
        }
    }

    result[j] = '\0';
    return result;
}

// Writes the path to the DTNChat home directory (in $XDG_DATA_HOME), creating it and its subdirectory if they don't exist.
char *get_dtnchat_home_dir(void) {
    static char *home_dir = NULL;
    const char *envval;
    int result;
    char *subpath;

    if (home_dir)
        return home_dir;

    home_dir = malloc(MAX_PATH_LENGTH);

    envval = getenv("XDG_DATA_HOME");
    if (envval) {
        result = snprintf(home_dir, MAX_PATH_LENGTH, "%s/dtnchat", envval);
    } else {
        envval = getenv("HOME");
        result = snprintf(home_dir, MAX_PATH_LENGTH, "%s/.local/share/dtnchat", envval);
    }

    if (result >= MAX_PATH_LENGTH || result < 0) return NULL;

    if (mkdir(home_dir, 0700) < 0 && errno != EEXIST) {
        perror("Couldn't create DTNChat home directory");
        return NULL;
    }

    subpath = malloc(strlen(home_dir) + 256);

    strcpy(subpath, home_dir);
    strcpy(subpath + strlen(home_dir), "/photo");

    if (mkdir(subpath, 0700) < 0 && errno != EEXIST) {
        perror("Couldn't create /photo subdirectory in DTNChat home");
        return NULL;
    }

    strcpy(subpath + strlen(home_dir), "/audio");

    if (mkdir(subpath, 0700) < 0 && errno != EEXIST) {
        perror("Couldn't create /audio subdirectory in DTNChat home");
        return NULL;
    }

    strcpy(subpath + strlen(home_dir), "/file");

    if (mkdir(subpath, 0700) < 0 && errno != EEXIST) {
        perror("Couldn't create /file subdirectory in DTNChat home");
        return NULL;
    }

    strcpy(subpath + strlen(home_dir), "/msg");
    if (mkdir(subpath, 0700) < 0 && errno != EEXIST) {
        perror("Couldn't create /msg subdirectory in DTNChat home");
        return NULL;
    }

    free(subpath);

    return home_dir;
}

char *get_file_name_from_path(char *path) {
    char *name;

    for (name = path + strlen(path) - 1; *name != '/' && name >= path ; name--);

    return name + 1;
}

static int get_time_filename(char *dest, size_t max_len) {
    size_t current_len;
    struct timespec tp_now;
    struct tm *tm_now;

    clock_gettime(CLOCK_REALTIME, &tp_now);
    tm_now = localtime(&tp_now.tv_sec);
    current_len = strftime(dest, max_len - 9, "%FT%T", tm_now);
    if (current_len == 0) return 0;

    return snprintf(dest + current_len, max_len - current_len, ".%09ld", tp_now.tv_nsec);
}

char *get_subdir_path(const char *subdir, const char *eid) {
    char *home_dir_path;
    static char subdir_path[4096];
    char *escaped_eid;

    home_dir_path = get_dtnchat_home_dir();
    if (!home_dir_path)
        return NULL;

    escaped_eid = escape_text(eid, "/");

    sprintf(subdir_path, "%s/%s/%s", home_dir_path, subdir, escaped_eid);

    free(escaped_eid);

    return subdir_path;
}

static int get_path(char **dest, const char *subdir, const char *eid, const char *extension) {
    char *subdir_path;
    char file_name[256];            // 2022-01-08T09:30:43
    static char file_path[4352];    // $XDG_DATA_DIR/dtnchat/photo/2022-01-08T09:30:43.png
    int result;

    subdir_path = get_subdir_path(subdir, eid);
    if (!subdir_path)
        return -1;

    if ((result = get_time_filename(file_name, sizeof(file_name))) >= sizeof(file_name) || result < 0) {
        return -2;
    }

    snprintf(file_path, sizeof(file_path), "%s/%s%s", subdir_path, file_name, extension ? extension : "");

    *dest = file_path;

    return 0;
}

int get_path_for_photo(char **dest, const char *eid) {
    return get_path(dest, "photo", eid, ".jpeg");
}

int get_path_for_audio(char **dest, const char *eid) {
    return get_path(dest, "audio", eid, ".ogg");
}

int get_path_for_file(char **dest, const char *eid) {
    return get_path(dest, "file", eid, NULL);
}

int get_msg_dir_path(char **dest) {
    char *home_dir_path;
    static char msg_path[4096];

    home_dir_path = get_dtnchat_home_dir();

    if (!home_dir_path)
        return -1;

    strcpy(msg_path, home_dir_path);
    strcat(msg_path, "/msg");

    *dest = msg_path;

    return 0;
}

// The returned pointer must be freed by the caller
static char *get_existing_path(const char *filename, const char *subdir, const char *eid) {
    char *subdir_path, *result;

    subdir_path = get_subdir_path(subdir, eid);
    if (!subdir_path)
        return NULL;

    result = malloc(strlen(subdir_path) + strlen(filename) + 2);
    sprintf(result, "%s/%s", subdir_path, filename);

    return result;
}

char *get_existing_photo_path(const char *filename, const char *eid) {
    return get_existing_path(filename, "photo", eid);
}

char *get_existing_audio_path(const char *filename, const char *eid) {
    return get_existing_path(filename, "audio", eid);
}

char *get_existing_file_path(const char *filename, const char *eid) {
    return get_existing_path(filename, "file", eid);
}

// Turns a string such as "physical/logical" into "physical\0logical"
// and returns a pointer to the character that followed the slash
char *split_file_string_pair(char *str) {
    int i;

    for (i = 0; str[i] && str[i] != '/'; i++);

    if (str[i] != '/') {
        return NULL;
    } else {
        str[i] = '\0';
        return &str[i + 1];
    }
}

void show_error_dialog(GtkWidget *widget, const char *msg) {
    GtkWidget *err_dialog = gtk_message_dialog_new(GTK_WINDOW(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW)),
                                                   GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                                   GTK_MESSAGE_ERROR,
                                                   GTK_BUTTONS_CLOSE,
                                                   "%s",
                                                   msg);
    gtk_dialog_run(GTK_DIALOG(err_dialog));
    gtk_widget_destroy(err_dialog);
}
