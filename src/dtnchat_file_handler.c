#include <gtk/gtk.h>

#include <dtnchat_file_handler.h>
#include <dtnchat_save_file_button.h>
#include <util.h>

struct _DTNChatFileHandler {
    GtkBox parent;
    GtkWidget *filename_label;
    GtkWidget *open_button;
    GtkWidget *save_button;

    char *file_path;
    char *logical_filename;
};

G_DEFINE_TYPE(DTNChatFileHandler, dtnchat_file_handler, GTK_TYPE_BOX);

typedef enum {
    PROP_FILE_PATH = 1,
    PROP_LOGICAL_FILENAME,
    N_PROP
} DTNCHAT_FILE_HANDLER_PROP;

static void dtnchat_file_handler_init(DTNChatFileHandler *handler) {
    gtk_widget_init_template(GTK_WIDGET(handler));
}

static void dtnchat_file_handler_set_property(GObject *obj, guint property_id, const GValue *val, GParamSpec *pspec) {
    DTNChatFileHandler *handler = DTNCHAT_FILE_HANDLER(obj);

    switch (property_id) {
        case PROP_FILE_PATH:
            g_free(handler->file_path);
            handler->file_path = g_value_dup_string(val);
            break;

        case PROP_LOGICAL_FILENAME:
            g_free(handler->logical_filename);
            handler->logical_filename = g_value_dup_string(val);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_file_handler_get_property(GObject *obj, guint property_id, GValue *val, GParamSpec *pspec) {
    DTNChatFileHandler *handler = DTNCHAT_FILE_HANDLER(obj);

    switch (property_id) {
        case PROP_FILE_PATH:
            g_value_set_string(val, handler->file_path);
            break;

        case PROP_LOGICAL_FILENAME:
            g_value_set_string(val, handler->logical_filename);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
    }
}

static void dtnchat_file_handler_constructed(GObject *obj) {
    DTNChatFileHandler *handler = DTNCHAT_FILE_HANDLER(obj);

    gtk_label_set_label(GTK_LABEL(handler->filename_label), handler->logical_filename);

    handler->save_button = GTK_WIDGET(dtnchat_save_file_button_new(handler->file_path, handler->logical_filename, FALSE));
    gtk_container_add(GTK_CONTAINER(handler), handler->save_button);
    gtk_widget_show(handler->save_button);
}

static void dtnchat_file_handler_finalize(GObject *obj) {
    DTNChatFileHandler *handler = DTNCHAT_FILE_HANDLER(obj);

    g_free(handler->file_path);
}

static void dtnchat_file_handler_class_init(DTNChatFileHandlerClass *class) {
    GParamSpec *props[N_PROP] = { NULL, };

    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class), "/it/unibo/dtnchat/file_handler.ui");
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatFileHandler, filename_label);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatFileHandler, open_button);

    G_OBJECT_CLASS(class)->set_property = dtnchat_file_handler_set_property;
    G_OBJECT_CLASS(class)->get_property = dtnchat_file_handler_get_property;
    G_OBJECT_CLASS(class)->constructed = dtnchat_file_handler_constructed;
    G_OBJECT_CLASS(class)->finalize = dtnchat_file_handler_finalize;

    props[PROP_FILE_PATH] = g_param_spec_string("file-path",
                                                "FilePath",
                                                "Path of the file to handle",
                                                NULL,
                                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

    props[PROP_LOGICAL_FILENAME] = g_param_spec_string("logical-filename",
                                                       "LogicalFilename",
                                                       "Filename to show to the user",
                                                       NULL,
                                                       G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

    g_object_class_install_properties(G_OBJECT_CLASS(class), N_PROP, props);

    g_param_spec_unref(props[PROP_FILE_PATH]);
    g_param_spec_unref(props[PROP_LOGICAL_FILENAME]);
}

DTNChatFileHandler *dtnchat_file_handler_new(const char *file_path, const char *logical_filename) {
    return g_object_new(DTNCHAT_FILE_HANDLER_TYPE,
                        "file-path", file_path,
                        "logical-filename", logical_filename,
                        NULL);
}

void dtnchat_file_handler_open(DTNChatFileHandler *handler) {
    const char *uri_prefix = "file://";
    char *uri;

    uri = malloc(strlen(uri_prefix) + strlen(handler->file_path) + 1);
    strcpy(uri, uri_prefix);
    strcat(uri, handler->file_path);
    gtk_show_uri_on_window(GTK_WINDOW(gtk_widget_get_ancestor(GTK_WIDGET(handler), GTK_TYPE_WINDOW)), uri, time(NULL), NULL);
    free(uri);
}
