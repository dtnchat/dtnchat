#include <gtk/gtk.h>
#include <stdio.h>

#include <dtnchat_recipient_dialog.h>
#include <dtnchat_window.h>

struct _DTNChatRecipientDialog {
    GtkDialog parent;
    DTNChatWindow *main_window;
    GtkWidget *box;
    GtkWidget *label;
    GtkWidget *entry;
    GtkWidget *button_cancel;
    GtkWidget *button_ok;
};

G_DEFINE_TYPE(DTNChatRecipientDialog, dtnchat_recipient_dialog, GTK_TYPE_DIALOG);

static void dtnchat_recipient_dialog_init(DTNChatRecipientDialog *dialog) {
    gtk_widget_init_template(GTK_WIDGET(dialog));
}

static void dtnchat_recipient_dialog_class_init(DTNChatRecipientDialogClass *class) {
    gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class), "/it/unibo/dtnchat/recipient_dialog.ui");
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecipientDialog, box);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecipientDialog, label);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecipientDialog, entry);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecipientDialog, button_cancel);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class), DTNChatRecipientDialog, button_ok);
}

DTNChatRecipientDialog *dtnchat_recipient_dialog_new() {
    return g_object_new(DTNCHAT_RECIPIENT_DIALOG_TYPE,
            "use-header-bar", 1,
            NULL);
}

// Enables/disables the "ok" button based on the syntactic validity of the eid in the entry
void dtnchat_recipient_dialog_update_ok(DTNChatRecipientDialog *dialog) {
    gboolean ok = FALSE;
    const gchar *text = gtk_entry_get_text(GTK_ENTRY(dialog->entry));
    int len = strlen(text);

    if (len >= 7) { // There needs to be at least either dtn://x/x or ipn:x.x

        if (strncmp(text, "dtn://", 6) == 0) {
            if (len >= 9) { // must be at least dtn://x/x
                // make sure there is at least one slash followed by at least one character
                for (int i = 7; text[i + 1]; i++) {
                    if (text[i] == '/') {
                        ok = TRUE;
                        break;
                    }
                }
            }

        } else if (strncmp(text, "ipn:", 4) == 0) {
            // make sure there is one dot and one only, followed by at least one character,
            // and everything else is numeric
            gboolean dot_found = FALSE;
            for (int i = 5; text[i]; i++) {
                if (text[i] != '.' && (text[i] < '0' || text[i] > '9')) { // invalid character
                    ok = FALSE;
                    break;
                }
                if (text[i] == '.') {
                    if (dot_found) { // more than one dot
                        ok = FALSE;
                        break;
                    } else { // first dot
                        ok = TRUE;
                        dot_found = TRUE;
                    }
                }
            }
        }

    }

    gtk_widget_set_sensitive(dialog->button_ok, ok);
}

// Returns the entry
GtkEntry *dtnchat_recipient_dialog_get_entry(DTNChatRecipientDialog *dialog) {
    return GTK_ENTRY(dialog->entry);
}
