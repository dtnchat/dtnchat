# DTNchat

A GTK3 messaging application based on the [Unified
API](https://gitlab.com/dtnsuite/unified_api), a library with the goal of
abstracting the most common DTN implementations.

# Copyright

Copyright (c) 2021, University of Bologna.

# License

DTNchat is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

DTNchat is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with DTNchat. If not, see <https://www.gnu.org/licenses/>.


## Dependencies

DTNchat depends on the following libraries:

* At least one of the DTN implementations supported by the Unified API:
  [DTN2](https://github.com/delay-tolerant-networking/DTN2), [ION](https://sourceforge.net/projects/ion-dtn/),
  [IBR-DTN](https://github.com/ibrdtn/ibrdtn), [µD3TN](https://gitlab.com/d3tn/ud3tn), [Unibo-BP](https://gitlab.com/unibo-dtn/unibo-bp) 

* The [Unified API](https://gitlab.com/dtnsuite/unified_api)

* GTK 3 (`libgtk-3-dev` on Debian)

* GStreamer 1.0 (`libgstreamer1.0-dev` on Debian)

* libcheese and libcheese-gtk (`libcheese-dev` and `libcheese-gtk-dev` on Debian)

## Build

You can learn about the available building configurations by running `make help`. The `*_DIR` variables must point to
the directories where the respective libraries have been built.

For instance, if you want to build DTNchat for ION, first build ION and the Unified API, then run:

```sh
$ make ION_DIR=/path/to/ion-open-source-x.y.z UNIFIED_API_DIR=/path/to/unified_api
```

You can combine multiple variables to build DTNchat for multiple DTN implementations.

After building, the executable will be available in the `build` subdirectory. Before running it please make sure at
least one of the DTN implementations it was built for is already running.

## Install

To install DTNchat to a system path, run

```sh
$ sudo make install
```

## Data storage

When run, DTNchat will store all your conversations and sent/received files in `~/.local/share/dtnchat` (or
`$XDG_DATA_HOME/dtnchat` if set). You can delete or move the entire directory to make DTNchat forget everything.
