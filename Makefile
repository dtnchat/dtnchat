#Makefile for compiling DTNchat
# NAME OF BIN USED FOR INSTALL/UNINSTALL (NEVER LEAVE IT BLANK!!!)
BIN_NAME_BASE=dtnchat

APP_DIRS=src/*.c
LIB_SYS= -lpthread  #Additional system libraries
LIB_PATHS = -L/usr/local/lib
APP_INC= -Iinclude

## DTNchat specific
#write the path of pkg-config into a variable:
PKGCONFIG = $(shell which pkg-config)
#write the path of glib-compile-resources into a variable:
GLIB_COMPILE_RESOURCES = $(shell $(PKGCONFIG) --variable=glib_compile_resources gio-2.0)
#write the linking flags for external libraries into a variable (e.g. -lglib-2.0)
LIB_SYS += $(shell $(PKGCONFIG) --libs glib-2.0 gtk+-3.0 gstreamer-1.0 cheese cheese-gtk)
#write the include flags for external libraries into a variable (e.g. -I/usr/include/glib-2.0)
INC_OTHERLIBS = $(shell $(PKGCONFIG) --cflags glib-2.0 gtk+-3.0 gstreamer-1.0 cheese cheese-gtk)

#Source filenames (e.g. src/main.c)
SRC = $(wildcard src/*.c)
#Object filenames (e.g. build/main.o), plus build/resources.o
OBJ = $(SRC:src%.c=build%.o) build/resources.o
#Glade XML filenames (e.g. resources/window.ui)
UI = $(wildcard resources/*.ui)

#Expose function names for GTK Builder
LDFLAGS = -rdynamic
## End DFTNchat specific

CC=gcc
UNIFIED_API_LIB_PATH=-L/usr/local/lib -L$(UNIFIED_API_DIR)
#UNIFIED_API_LIB_PATH=-L$(UNIFIED_API_DIR)
UNIFIED_API_INC= -I$(UNIFIED_API_DIR)/src
INSTALL_PATH=/usr/local/bin

DEBUG=0
ifeq ($(DEBUG),0)
DEBUG_FLAG=-O2
else
DEBUG_FLAG=-g -fno-inline -O0
endif
CFLAGS= $(DEBUG_FLAG) -Wall -fmessage-length=0 -Werror


INSTALLED=$(wildcard $(INSTALL_PATH)/$(BIN_NAME_BASE)_*)

HAVE_IMPL := 0

BIN_NAME=$(BIN_NAME_BASE)
UNIFIED_API_LIB_NAME=libunified_api

ifneq ($(strip $(DTN2_DIR)),)
# DTN2
HAVE_IMPL := 1
LIB_BP := $(LIB_BP)  -ldtnapi -ltirpc
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vDTN2
BIN_NAME := $(BIN_NAME)_vDTN2
endif

ifneq ($(strip $(ION_DIR)),)
# ION
HAVE_IMPL := 1
LIB_BP := $(LIB_BP) -lbp -lici 
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vION
BIN_NAME := $(BIN_NAME)_vION
endif

ifneq ($(strip $(IBRDTN_DIR)),)
# IBRDTN
HAVE_IMPL := 1
#IBRDTN_VERSION=1.0.1
LIB_BP := $(LIB_BP) -librcommon -librdtn -lz
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vIBRDTN
BIN_NAME := $(BIN_NAME)_vIBRDTN
endif

ifneq ($(strip $(UD3TN_DIR)),)
# UD3TN
HAVE_IMPL := 1
LIB_BP := $(LIB_BP)
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vUD3TN
BIN_NAME := $(BIN_NAME)_vUD3TN
endif

ifeq ($(UNIBO_BP),1)
# UD3TN
HAVE_IMPL := 1
LIB_BP := $(LIB_BP)  -lunibo-bp-api
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vUNIBOBP
BIN_NAME := $(BIN_NAME)_vUNIBOBP
endif

ifeq ($(strip $(UNIFIED_API_DIR)),)
# Missing UNIFIED_API_DIR
all: help
else ifeq ($(HAVE_IMPL),0)
# Missing all implementations
all: help
else
# Ok
all: bin
endif



UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME).a

UNIFIED_API_LIB=$(UNIFIED_API_DIR)/$(UNIFIED_API_LIB_NAME) #Independent of BP implementation


# building build/filename.o requires src/filename.c to exist
build/%.o: src/%.c
#	create build directory if it doesn't exist (@ to hide the command from the output)
	@mkdir -p build
#	compile source file into object ($@ = build/filename.o, $< = src/filename.c)
	$(CC) $(UNIFIED_API_INC) $(APP_INC) $(INC_OTHERLIBS) $(CFLAGS) -o $@ -c $<

# building build/resources.c requires resources/dtnchat.gresource.xml and UI files to exist
build/resources.c: resources/dtnchat.gresource.xml $(UI)
	@mkdir -p build
	$(GLIB_COMPILE_RESOURCES) resources/dtnchat.gresource.xml --target=$@ --sourcedir=resources --generate-source

# building build/resources.o requires build/resources.c to exist
build/resources.o: build/resources.c
	$(CC) $(INC_OTHERLIBS) $(CFLAGS) -o $@ -c $<

# building the binary requires to call the target (build/%.o) for each object file
# the target "bin" depends on the targets contained on $(OBJ), i.e. "build/%.o", where % stands for "any file name"
bin: $(OBJ)
	$(CC) $(LIB_PATHS) $(LDFLAGS) $(OBJ) $(UNIFIED_API_LIB) $(LIB_SYS) $(LIB_BP) -o build/$(BIN_NAME)
#	create symlink "build/dtnchat" to the binary that's just been built (e.g. build/dtnchat -> build/dtnchat_vION)
#	needed to have a unified "dtnchat" command for the desktop environment menu entry
	ln -sf $(BIN_NAME) build/dtnchat

install:
	cp $(filter-out build/%.c build/%.o, $(wildcard build/$(BIN_NAME_BASE)*)) $(INSTALL_PATH)/
#	create symlink to installed binary (e.g. /usr/local/bin/dtnchat -> /usr/local/bin/dtnchat_vION)
	ln -sf $$(basename $$(readlink -f build/dtnchat)) $(INSTALL_PATH)/dtnchat
#	copy application icons into system directories
	for size in 16 32 48 64 128 256 ; do \
		install -Dm644 resources/icon_$${size}.png /usr/share/icons/hicolor/$${size}x$${size}/apps/it.unibo.DTNchat.png ; \
	done
#	copy desktop environment menu entry
	install -Dm644 resources/it.unibo.DTNchat.desktop /usr/share/applications

uninstall:
	@if test `echo $(INSTALLED) | wc -w` -eq 1 -a -f "$(INSTALLED)"; then rm -rf $(INSTALLED); else echo "MORE THAN 1 FILE, DELETE THEM MANUALLY: $(INSTALLED)"; fi
	@rm -f /usr/local/bin/dtnchat
	@rm -f /usr/share/applications/it.unibo.DTNchat.desktop
	@rm -f /usr/share/icons/hicolor/*/apps/it.unibo.DTNchat.png

help:
	@echo "Usage:"
	@echo "For DTN2:                make DTN2_DIR=<dtn2_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For ION:                 make ION_DIR=<ion_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For IBRDTN:              make IBRDTN_DIR=<ibrdtn_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For UD3TN:               make UD3DTN_DIR=<ud3tn_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For Unibo-BP:            make UNIBO_BP=1 UNIFIED_API_DIR=<al_bp_dir>"
	@echo "  The implementation dirs (DTN2_DIR, ION_DIR, ...) can be combined to make"
	@echo "  an executable that runs with multiple BP implementations."
	@echo "To compile with debug symbols add DEBUG=1"

clean:
	rm -rf build/*



